<!--Header Top-->
<div class="header-top blue-bg-2">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                <div class="single-header-top">
                    <!-- <p>
                        <span>MinesRealm, King Charles Str, London,, SW1A2AH, United Kingdom!</span>
                        <span><i class="fa fa-phone"></i> (+44) premium only</span>
                    </p> -->
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                <div class="single-header-top last">
                    <div class="account-menu">
                        <select name="country">
                            <option value="">Eng</option>
                            <option value="">Fra</option>
                            <option value="">Spn</option>
                            <option value="">Ger</option>
                        </select>
                        <ul>
                            <!-- <li><a href="">Support</a></li> -->
                            <li><a href="/login/"><i class="fa fa-key"></i>Login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div><!--/Header Top-->
<header class="header-area blue-bg">
    <nav class="navbar navbar-expand-lg main-menu">
        <div class="container">
            <a class="navbar-brand" href="/"><img width="100px" src="/assets/images/2.jpeg" class="d-inline-block align-top" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="menu-toggle"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <!-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="blog.html" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Home</a> -->
                        <!-- <ul class="dropdown-menu"> -->
                            <!-- <li><a class="nav-item" href="index.php">Home</a></li> -->
                            
                        <!-- </ul> -->
                    <!-- </li> -->
                    <li class="nav-item"><a class="nav-link" href="/">Home</a></li>
                    
                    <!-- <li class="nav-item"><a class="nav-link" href="">Plans</a></li> -->
                    <li class="nav-item"><a class="nav-link" href="/about/">About us</a></li>
                    <li class="nav-item"><a class="nav-link" href="/faq/">FAQ</a></li>
                    <li class="nav-item"><a class="nav-link" href="/testimonials/">Testimonials</a></li>
                    <li class="nav-item"><a data-target="#calculator" data-toggle="modal" class="nav-link" href="javascript:void(0)">Interest</a></li>


                    <!-- <li class="nav-item"><a class="nav-link" href="">Support</a></li> -->
        <!-- 
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="about.html">About</a></li>
                            <li><a class="dropdown-item" href="contact.html">Contact</a></li>
                            <li><a class="dropdown-item" href="faq.html">FAQ page</a></li>
                            <li><a class="dropdown-item" href="testimonials.html">Success Stories</a></li>
                            <li><a class="dropdown-item" href="promotions.html">Promotions</a></li>
                            <li><a class="dropdown-item" href="how-to-invest.html">How to invest</a></li>
                            <li><a class="dropdown-item" href="affiliate.html">Affiliate</a></li>
                            <li><a class="dropdown-item" href="login.html">Login</a></li>
                            <li><a class="dropdown-item" href="404.html">Error page</a></li>
                            <li><a class="dropdown-item" href="privacy.html">Privacy</a></li>
                            <li><a class="dropdown-item" href="tos.html">Terms and Services</a></li>
                        </ul>
                    </li> -->
                    
                
                </ul>
                <div class="header-btn justify-content-end">
                    <a href="/profile/" class="bttn-small btn-fill"><i style="font-size:20px" class="fa fa-user"></i></a>
                    <a href="/logout/" class="bttn-small btn-fill"><i style="font-size:20px" class="fa fa-sign-out"></i></a>
                </div>
            </div>
        </div>
    </nav>
</header>