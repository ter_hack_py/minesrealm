<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=1024">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="We remain true to our word. By providing the best crypto currency services possible. Putting customers,partners and stake holders at the heart of our business.">
    <meta name="keywords" content="Investment, Cryptocurrency, Payment, Cashout">
    <meta name="author" content="minesrealm.com">
        
        
    <link rel="shortcut icon" href="../assets/images/2.jpeg" type="image/x-icon">
    <link rel="icon" href="../assets/images/2.jpeg" type="image/x-icon">

    <title>Minesrealm - Testimonials</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="../assets/css/magnific-popup.css" rel="stylesheet">
    <link href="../assets/css/jquery-ui.css" rel="stylesheet">


    <link href="../assets/css/animate.html" rel="stylesheet">
    <link href="../assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="../assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <!-- Preloader -->
    <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div>
    <!--/Preloader -->

    <!--Header Top-->
    <?php 
    require_once('../header.php'); 
    ?>
   <!--/Header Area-->



    <!-- Testimonials Area -->
    <section class="section-padding-2 blue-bg-2">
        <div class="container">
            <div class="row">
                <div class="col centered">
                    <div class="section-title cl-white">
                        <h4>Testimonials</h4>
                        <h2>Our Player Reviews</h2>
                    </div>
                </div>
            </div>
            <div class="testimonials owl-carousel">
                <div class="single-testimonial">
                    <div class="testimonial-thumb">
                        <img src="../assets/images/reviewer/1.jpg" alt="">
                    </div>
                    <div class="testimonial-content">
                        <div class="ratings">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                        <p>Wonderful service!! You can invest their plans right now! Site paying me fine. I absolutely trust with this company. Thanks for admin offer us a stable program! You are best!</p>
                        <div class="reviewer-meta">
                            <h5>IRMA BROCKINGTON MADISON</h5>
                            <h6>WISCONSIN</h6>
                        </div>
                    </div>
                </div>
                <div class="single-testimonial">
                    <div class="testimonial-thumb">
                        <img src="../assets/images/reviewer/2.jpg" alt="">
                    </div>
                    <div class="testimonial-content">
                        <div class="ratings">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                        <p>This is a serious paying investment program. and always pays instantly! Sent me payments without any problems. Many thanks to the admin. member</p>
                        <div class="reviewer-meta">
                            <h5>LUCAS FERRANTI</h5>
                            <h6>ORLANDO, FLORIDA</h6>
                        </div>
                    </div>
                </div>
                <div class="single-testimonial">
                    <div class="testimonial-thumb">
                        <img src="../assets/images/reviewer/3.jpg" alt="">
                    </div>
                    <div class="testimonial-content">
                        <div class="ratings">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                        <p>Great service! I have been worried about investing. But when I came here. I don't have to worry anymore. Excellent performance, they continue to make good commitments. Thank you!</p>
                        <div class="reviewer-meta">
                            <h5>LYNN SHEFFIELD WAVERLY</h5>
                            <h6>ILLINOIS</h6>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section><!-- /Testimonials Area -->   
     <!-- Affiliate Area -->
     <section class="section-padding blue-bg-2">
        <div class="container">
            <!-- <div class="row"> -->
                <!-- <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6"> -->
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                            <!-- <div class="single-box">
                               
                            </div> -->
                            <img src="../assets/images/1.jpeg" alt="">

                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                            <!-- <div class="single-box">
                               
                            </div> -->
                            <img src="../assets/images/2.jpeg" alt="">

                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                            <!-- <div class="single-box">
                                
                            </div> -->
                            <img src="../assets/images/3.jpeg" alt="">

                        </div>
                       
                    </div>
                <!-- </div> -->
              
            <!-- </div> -->
        </div>
    </section><!-- /Affiliate Area -->
        
    <!-- Newslatter -->
    <section class="section-padding blue-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 centered cl-white">
                    <div class="section-title">
                        <h4>Subscribe</h4>
                        <h2>Don't miss any update</h2>
                    </div>
                    <div class="newslatter">
                        <form action="#">
                            <input type="email" placeholder="Enter your email" required>
                            <button type="submit"><i class="fa fa-paper-plane"></i></button>
                        </form>
                        <p><i class="fa fa-info-circle"></i>We will never send spam</p>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /Newslatter -->
    
   

    <!--Footer Area -->
    <?php require_once('../footer.php') ?>
    <!--/Footer Area-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../assets/js/jquery-3.2.1.min.js"></script>
    <script src="../assets/js/jquery-migrate.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>

    <script src="../assets/js/popper.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/owl.carousel.min.js"></script>

    <script src="../assets/js/magnific-popup.min.js"></script>
    <script src="../assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="../assets/js/isotope.pkgd.min.js"></script>
    
    <script src="../assets/js/waypoints.min.js"></script>
    <script src="../assets/js/jquery.counterup.min.js"></script>
    <script src="../assets/js/wow.min.js"></script>
    <script src="../assets/js/scrollUp.min.js"></script>

    <script src="../assets/js/script.js"></script>
</body>
</html>