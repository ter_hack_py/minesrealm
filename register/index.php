<?php 

if($_SERVER["REQUEST_METHOD"] == "POST"){
    // $url = "/MinesRealm/register/";
    // $err = "";
    
    if (empty($_POST['email']) || empty($_POST['full_name']) || empty($_POST['phone'])) {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>Error, Empty Field </div>';
    }else {
        require_once("../server/db/index.php");
        
        $sql = "SELECT email FROM users WHERE email = ?";

        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_email);
            // Set parameters
            $param_email = trim($_POST["email"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);

                if(mysqli_stmt_num_rows($stmt) == 1){
                    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> <stong>Sorry! </strong>Email Already taken </div>';
                } else{
                    
                    // start
                    $full_name = htmlspecialchars($_POST['full_name']);
                    $username = htmlspecialchars($_POST['username']);
                    $email = htmlspecialchars($_POST['email']);
                    $pwd = htmlspecialchars($_POST['pwd']);
                    $phone = htmlspecialchars($_POST['phone']);
                    $s_qtn = htmlspecialchars($_POST['s_qtn']);
                    $s_ans = htmlspecialchars($_POST['s_ans']);
                    $payment = htmlspecialchars($_POST['payment']);
                    // echo "<script>alert('Okay')</script>";
                    // id	full_name	username	email	phone	pwd	s_qtn	s_ans	payment	reg_at
                    $sql = "INSERT INTO users (full_name, username, email, phone, pwd, s_qtn, s_ans, payment) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
                    if($stmt = mysqli_prepare($link, $sql)){
                        // Bind variables to the prepared statement as parameters
                        mysqli_stmt_bind_param($stmt, "ssssssss", $param_full_name, $param_username, $param_email, $param_phone, $param_pwd, $param_s_qtn, $param_s_ans, $param_payment);
                        // Set parameters
                        $param_full_name = $full_name;
                        $param_username = $username;
                        $param_email = $email;
                        $param_phone = $phone;
                        $param_pwd = password_hash($pwd, PASSWORD_DEFAULT);
                        $param_s_qtn = $s_qtn;
                        $param_s_ans = $s_ans;
                        $param_payment = $payment;
                        
                        // Attempt to execute the prepared statement
                        if(mysqli_stmt_execute($stmt)){
                            session_start();
                            $_SESSION['email'] = $param_email;
                            $_SESSION['pwd'] = $pwd;

                            echo '<div class="alert alert-success alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button><strong>Completed! </strong> Registered Succesfully, login as, ' . '<strong>'. $full_name .' ?</strong>' . '  <a href="../login/"><button type="button" class="btn btn-info">LOGIN</button></a></div>';
                        } else{
                            echo "Something went wrong. Please try again later.";
                        }
                    }
                     // Close statement
                     mysqli_stmt_close($stmt);
                }
                // Close connection
                 mysqli_close($link);

            }
        }
    }
}
 ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=1024">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="We remain true to our word. By providing the best crypto currency services possible. Putting customers,partners and stake holders at the heart of our business.">
    <meta name="keywords" content="Investment, Cryptocurrency, Payment, Cashout">
    <meta name="author" content="minesrealm.com">
        
        
    <link rel="shortcut icon" href="../assets/images/2.jpeg" type="image/x-icon">
    <link rel="icon" href="../assets/images/2.jpeg" type="image/x-icon">
        
        
   

    <title>Mines Realm</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="../assets/css/magnific-popup.css" rel="stylesheet">
    <link href="../assets/css/jquery-ui.css" rel="stylesheet">


    <link href="../assets/css/animate.html" rel="stylesheet">
    <link href="../assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="../assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <!-- Preloader -->
    <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div>
    
    <!--/Preloader -->

    <!--Header Top-->
   <?php require_once("../header.php") ?>
    <!--/Header Area-->
    
   

    <!--Signup Section -->
    <section class="section-padding blue-bg shaded-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <div class="account-form cl-white">
                        <div class="title">
                            <h3>Create your account</h3>
                        </div>

                        <form  method="post">
                            <div class="row">
                                <div class="col-xl-6">
                                    <input name="full_name" id="full_name" type="text" placeholder="Your Full Name" require>
                                </div>
                                <div class="col-xl-6">
                                    <input name="username" id="username" type="text" placeholder="Choose a Username" require>
                                </div>
                                <div class="col-xl-12">
                                    <input name="email" id="email" type="email" placeholder="Your E-Mail Address" require>
                                </div>
                                <!-- <div class="col-xl-3">
                                    <select class="form-control">
                                        <option><img src="https://www.countryflags.io/be/flat/64.png"></option>
                                        <option><img src="https://www.countryflags.io/be/flat/64.png"></option>

                                    </select>
                                </div> -->
                                <div class="col-xl-12">
                                    <input name="phone" id="phone" type="number" placeholder="Your mobile number" require>
                                </div>
                                <div class="col-xl-12">
                                    <input name="pwd" id="pwd" type="password" placeholder="Define Password" require>
                                </div>
                                <div class="col-xl-12">
                                    <input name="s_qtn" id="s_qtn" type="text" placeholder="Secret Question" require>
                                </div>
                                <div class="col-xl-12">
                                    <input name="s_ans" id="s_ans" type="text" placeholder="Secret Answer" require>
                                </div>
                                <div class="col-xl-12">
                                    <input name="payment" id="payment" type="text" placeholder="Payment Account:Your Bitcoin Account" require>
                                </div>
                                <div class="col-xl-12">
                                    <p>By signing up to Mines Realm you confirm that you agree with the <a href="javascript:void(0)" data-toggle="modal" data-target="exampleModalCenter">Member terms and conditions</a></p>
                                </div>
                                <div class="col-xl-12">
                                    <button name="sign_up_user" type="submit" class="bttn-small btn-fill w-100">Create my account</button>
                                </div>
                                <div class="col-xl-12">
                                    <p><a href="/login/">Do you already have an account?</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/Signup Section-->

    <!--Footer Area -->
    <?php require_once("../footer.php") ?>
   <!--/Footer Area-->


    <!-- request -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="../assets/js/request.js"></script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../assets/js/jquery-3.2.1.min.js"></script>
    <script src="../assets/js/jquery-migrate.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>

    <script src="../assets/js/popper.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/owl.carousel.min.js"></script>

    <script src="../assets/js/magnific-popup.min.js"></script>
    <script src="../assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="../assets/js/isotope.pkgd.min.js"></script>
    
    <script src="../assets/js/waypoints.min.js"></script>
    <script src="../assets/js/jquery.counterup.min.js"></script>
    <script src="../assets/js/wow.min.js"></script>
    <script src="../assets/js/scrollUp.min.js"></script>

    <script src="../assets/js/script.js"></script>
</body>

</html>