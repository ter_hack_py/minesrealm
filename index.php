<?php 
// define('DB_SERVER', 'localhost');
// define('DB_USERNAME', 'root');
// define('DB_PASSWORD', '');
// define('DB_NAME', 'minerealm');

// live server
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'minesrea_admin');
define('DB_PASSWORD', '%G^q;~_E8E7x');
define('DB_NAME', 'minesrea_server');   

/* Attempt to connect to MySQL database */
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="We remain true to our word. By providing the best crypto currency services possible. Putting customers,partners and stake holders at the heart of our business.">
    <meta name="keywords" content="Investment, Cryptocurrency, Payment, Cashout">
    <meta name="author" content="minesrealm.com">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta name="viewport" content="width=1024">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        
    <link rel="shortcut icon" href="assets/images/2.jpeg" type="image/x-icon">
    <link rel="icon" href="assets/images/2.jpeg" type="image/x-icon">

    <title>Mines Realm - Home</title>
    <!-- Bootstrap -->

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/magnific-popup.css" rel="stylesheet">
    <link href="assets/css/jquery-ui.css" rel="stylesheet">

    <link href="assets/css/animate.html" rel="stylesheet">
    <link href="assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="assets/css/main.css" rel="stylesheet">

    
</head>
<body>

    <!-- Preloader -->
    <!-- <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div> -->
    <!-- /Preloader -->

    
    <!--Header Area-->
   <?php require_once("header.php") ?>
    
    <!--/Header Area-->
    <!--Hero Area-->
    <!-- hero-area gradient-overlay -->
    <section class="gradient-overlay" style="background: url('assets/images/bg-new.jpeg') no-repeat center center; color:white; height:400px">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12">
                    <div class="hero-content"><br><br>
                        <h4>Minesrealm</h4>
                        <h1>Committed to effective and efficient investment satisfaction</h1>
                        <div class="hero-btn">
                            <a href="/login/" class="bttn-mid btn-fill mr-10">Invest now</a>
                            <a href="/login/" class="bttn-mid btn-blu">How to invest?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/Hero Area-->
    <!--How it works Section-->
    <section class="section-padding blue-bg shaded-bg" >
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 centered">
                    <div class="section-title cl-white">
                        <h4>Why us</h4>
                        <h4>What makes us different</h4>
                    </div>
                </div>
            </div>
            <!-- Registered Company     Crypto Currency -->
            <div class="row justify-content-center centered">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <div class="single-box">
                        <img src="assets/images/icons/1.png" alt="">
                        <h3>Security</h3>
                        <p>A better way to present your money using fully featured digital</p>
                        <a href="/login/" class="bttn-small btn-fill">Learn more</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <div class="single-box">
                        <img src="assets/images/icons/2.png" alt="">
                        <h3>Instant Money</h3>
                        <p>Receive payment immediately upon request!</p>
                        <a href="/login/" class="bttn-small btn-fill">Learn more</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <div class="single-box">
                        <img src="assets/images/icons/3.png" alt="">
                        <h3>Trust</h3>
                        <p>Minesrealm is an officially registered company</p>
                        <a href="/login/" class="bttn-small btn-fill">Learn more</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col">
                    <div class="single-box">
                        <img src="assets/images/icons/4.png" alt="">
                        <h3>Transparency</h3>
                        <p>We are not just another business launched for revenue motives</p>
                        <a href="/login/" class="bttn-small btn-fill">Learn more</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <div class="single-box">
                        <img src="assets/images/icons/5.png" alt="">
                        <h3>Awesome Withdraw</h3>
                        <p>Our team  provides the best quality service.</p>
                        <a href="/login/" class="bttn-small btn-fill">Learn more</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <div class="single-box">
                        <img src="assets/images/icons/6.png" alt="">
                        <h3>Accessibility</h3>
                        <p>we also aim to assist the intermediates & even expert traders </p>
                        <a href="/login/" class="bttn-small btn-fill">Learn more</a>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/How it works Section-->
    
    <!--About Section-->
    <section class="section-padding-2 blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-5 col-5">
                    <div class="about-img">
                        <img id="myimage" src="assets/images/1.jpeg" hei alt="">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-7 col-7">
                    <div class="about-content cl-white">
                        <div class="section-title">
                            <!-- <h4>Excellent History of us</h4> -->
                            <h2>About us</h2>
                        </div>
                        <p>With the advancement in technology & current world trends, the conventional means of money & trading have been greatly altered. The same place has been taken by the stock market, foreign exchange & cryptocurrency.....</p>
                       
                        <a href="/about/" class="bttn-mid btn-fill mt-10">Learn more</a>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/About Section-->

    
    <!--Best Plans Section-->
    <section class="section-padding blue-bg shaded-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>Best Plans</h4>
                        <h2>Choose Your plan</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <div class="single-playnow">
                        <img src="assets/images/play/1.png" alt="">
                        <h4><span class="bage"></span>Beginners plan</h4>
                        <p>$100 - $2,000</p>
                        <p>2% daily ROI</p>
                        <p>Min: 5days</p>
                        <a href="/profile/deposit/" class="bttn-small btn-fill">Invest Now</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <div class="single-playnow">
                        <img src="assets/images/play/2.png" alt="">
                        <h4>Regular plan</h4>
                        <p>$2,000 - $4,000</p>
                        <p>4% daily ROI</p>
                        <p>Min: 5days</p>
                        <a href="/profile/deposit/" class="bttn-small btn-fill">Invest Now</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <div class="single-playnow">
                        <img src="assets/images/play/3.png" alt="">
                        <h4>Pro plan</h4>
                        <p>$4,000 - $6,000</p>
                        <p>6% daily ROI</p>
                        <p>Min: 5days</p>
                        <a href="/profile/deposit/" class="bttn-small btn-fill">Invest Now</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    <div class="single-playnow">
                        <img src="assets/images/play/5.png" alt="">
                        <h4>Premium plan</h4>
                        <p>$8,000 - $1,000,000</p>
                        <p>8% daily ROI</p>
                        <p>Min: 5days</p>
                        <a href="/profile/deposit/" class="bttn-small btn-fill">Invest Now</a>
                    </div>
                </div>
               
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <a href="/register/" class="bttn-mid btn-fill">Get started</a>
                </div>
            </div>
        </div>
    </section><!--/Best Plans Section-->

    <!--Winner List-->
    
        
    <section class="section-padding blue-bg shaded-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>Weekly Update</h4>
                        <h2>Lastest Deposit update</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 mb-30">
                    <div class="winner-list table-responsive">
                        <table class="table table-dark table-hover table-striped table-borderless">
                            <thead>
                              <tr>
                                <!-- <th scope="col">Top</th> -->
                                <th scope="col">Name</th>
                                <th scope="col">ID</th>
                                <th scope="col">Deposit</th>
                                <th scope="col">History</th>
                                <th scope="col">Action</th>

                              </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $sql = "SELECT deposit, i_name, investor_id FROM investors WHERE i_type = 1 ORDER BY id desc LIMIT 10";
                            $result = mysqli_query($link, $sql);
                            if (mysqli_num_rows($result) > 0 )  {
                                while ($row = mysqli_fetch_array($result)) {
                                    ?>
                                    <tr>
                                    <!-- <th scope="row">1</th> -->
                                    <td class="cl-yellow"><?php echo ucfirst($row['i_name']) ?></td>
                                    <td><?php echo '#' . $row['investor_id']?></td>
                                    <td class="cl-red"><?php echo $row['deposit'] ?></td>
                                    <td><a href="/profile/" class="bttn-small btn-fill">Check now</a></td>
                                    <td><a href="/profile/" class="bttn-small btn-fill">Details</a></td>
                                    
                                  </tr>

                                  <?php
                                }
                            } else {
                                echo '<div class="col centered">
                                <h5>No record found</h5>
                            </div>';
                            }

                            ?>
                             
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col centered">
                    <a href="/profile/" class="bttn-mid btn-fill">Invest Now</a>
                </div>
            </div>
        </div>
    </section>
    <!--/Winner List-->
     <!--Winner List-->
     <!-- <section class="section-padding blue-bg shaded-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>Weekly Update</h4>
                        <h2>Lastest Deposit update</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 mb-30">
                    <div class="winner-list table-responsive">
                        <table class="table table-dark table-hover table-striped table-borderless">
                            <thead>
                              <tr>
                                <th scope="col">Top</th>
                                <th scope="col">Name</th>
                                <th scope="col">ID</th>
                                <th scope="col">Deposit</th>
                                <th scope="col">History</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <th scope="row">1</th>
                                <td class="cl-yellow"><img src="assets/images/commenter1.png" alt="">Philippe</td>
                                <td>#49588</td>
                                
                                <td class="cl-red">$1248.43</td>
                                <td><a href="/profile/" class="bttn-small btn-fill">Check now</a></td>
                              </tr>
                              <tr>
                                <th scope="row">2</th>
                                <td class="cl-yellow"><img src="assets/images/commenter1.png" alt="">Gab</td>
                                <td>#44433</td>
                                
                                <td class="cl-red">$550.39</td>
                                <td><a href="/profile/" class="bttn-small btn-fill">Check now</a></td>
                              </tr>
                              <tr>
                                <th scope="row">3</th>
                                <td class="cl-yellow"><img src="assets/images/commenter1.png" alt="">Bertschinger</td>
                                <td>#49588</td>
                                
                                <td class="cl-red">$874.56</td>
                                <td><a href="/profile/" class="bttn-small btn-fill">Check now</a></td>
                              </tr>
                              <tr>
                                <th scope="row">4</th>
                                <td class="cl-yellow"><img src="assets/images/commenter1.png" alt="">Hyelnaya</td>
                                <td>#49588</td>
                                
                                <td class="cl-red">$933.94</td>
                                <td><a href="/profile/" class="bttn-small btn-fill">Check now</a></td>
                              </tr>
                              <tr>
                                <th scope="row">5</th>
                                <td class="cl-yellow"><img src="assets/images/commenter1.png" alt="">Theoty</td>
                                <td>#49588</td>
                                
                                <td class="cl-red">$340.65</td>
                                <td><a href="/profile/" class="bttn-small btn-fill">Check now</a></td>
                              </tr>
                              <tr>
                                <th scope="row">6</th>
                                <td class="cl-yellow"><img src="assets/images/commenter1.png" alt="">Nrosal</td>
                                <td>#49588</td>
                                
                                <td class="cl-red">$2000.00</td>
                                <td><a href="/profile/" class="bttn-small btn-fill">Check now</a></td>
                              </tr>
                              <tr>
                                <th scope="row">7</th>
                                <td class="cl-yellow"><img src="assets/images/commenter1.png" alt="">Larry</td>
                                <td>#49588</td>
                                
                                <td class="cl-red">$1408.22</td>
                                <td><a href="/profile/" class="bttn-small btn-fill">Check now</a></td>
                              </tr>
                              <tr>
                                <th scope="row">8</th>
                                <td class="cl-yellow"><img src="assets/images/commenter1.png" alt="">Dave</td>
                                <td>#49588</td>
                                
                                <td class="cl-red">$250.41</td>
                                <td><a href="/profile/" class="bttn-small btn-fill">Check now</a></td>
                              </tr>
                              <tr>
                                <th scope="row">9</th>
                                <td class="cl-yellow"><img src="assets/images/commenter1.png" alt="">DMartins</td>
                                <td>#49588</td>
                                
                                <td class="cl-red">$680.99</td>
                                <td><a href="/profile/" class="bttn-small btn-fill">Check now</a></td>
                              </tr>
                              <tr>
                                <th scope="row">10</th>
                                <td class="cl-yellow"><img src="assets/images/commenter1.png" alt="">Haca</td>
                                <td>#49588</td>
                                
                                <td class="cl-red">$3600.09</td>
                                <td><a href="/profile/" class="bttn-small btn-fill">Check now</a></td>
                              </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col centered">
                    <a href="/profile/" class="bttn-mid btn-fill">Invest Now</a>
                </div>
            </div>
        </div>
    </section> -->
    <!--/Winner List-->
     <!--Winner List-->
     <section class="section-padding blue-bg shaded-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>Weekly Update</h4>
                        <h2>Lastest Withdrawal update</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 mb-30">
                    <div class="winner-list table-responsive">
                        <table class="table table-dark table-hover table-striped table-borderless">
                            <thead>
                              <tr>
                                <!-- <th scope="col">Top</th> -->
                                <th scope="col">Name</th>
                                <th scope="col">ID</th>
                                <th scope="col">Withdrawal</th>
                                <th scope="col">History</th>
                                <th scope="col">Action</th>

                              </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $sql = "SELECT withdrawal, i_name, investor_id FROM investors WHERE i_type = 2 ORDER BY id desc LIMIT 10";
                            $result = mysqli_query($link, $sql);
                            if (mysqli_num_rows($result) > 0 )  {
                                while ($row = mysqli_fetch_array($result)) {
                                    ?>
                                    <tr>
                                    <!-- <th scope="row">1</th> -->
                                    <td class="cl-yellow"><?php echo ucfirst($row['i_name']) ?></td>
                                    <td><?php echo '#' . $row['investor_id']?></td>
                                    <td class="cl-red"><?php echo $row['withdrawal'] ?></td>
                                    <td><a href="/profile/" class="bttn-small btn-fill">Check now</a></td>
                                    <td><a href="/profile/" class="bttn-small btn-fill">Details</a></td>
                                  </tr>
                                  <?php
                                }
                            }else {
                                echo '<div class="col centered">
                                <h5>No record found</h5>
                            </div>';
                            }

                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col centered">
                    <a href="/profile/" class="bttn-mid btn-fill">Invest Now</a>
                </div>
            </div>
        </div>
    </section><!--/Winner List-->

    <!--Payments Area-->
    <section class="section-padding-2 blue-bg shaded-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>Payment Methods</h4>
                        <!-- <h2>Even we accepts</h2> -->
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                    <div class="single-brands">
                        <a href="/profile/"><img src="assets/images/payment1.png" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                    <div class="single-brands">
                        <a href="/profile/"><img src="assets/images/payment2.png" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                    <div class="single-brands">
                        <a href="/profile/"><img src="assets/images/payment3.png" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                    <div class="single-brands">
                        <a href="/profile/"><img src="assets/images/payment4.png" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                    <div class="single-brands">
                        <a href="/profile/"><img src="assets/images/payment5.png" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                    <div class="single-brands">
                        <a href="/profile/"><img src="assets/images/payment6.png" alt=""></a>
                    </div>
                </div>
               
            </div>
            <div class="row justify-content-center">
               
            </div>
        </div>
    </section>
    <!--/Payments Area-->
    <!-- certificate area -->
    <section class="section-padding-2 blue-bg shaded-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>Certification</h4>
                        <!-- <h2>Even we accepts</h2> -->
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                    <div class="single-brands">
                        <a href="/profile/"><img src="assets/images/certified_logo_1.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                    <div class="single-brands">
                        <a href="/profile/"><img src="assets/images/certified_logo_2.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                    <div class="single-brands">
                        <a href="/profile/"><img src="assets/images/certified_logo_3.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                    <div class="single-brands">
                        <a href="/profile/"><img src="assets/images/certified_logo_4.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                    <div class="single-brands">
                        <a href="/profile/"><img src="assets/images/certified_logo_5.jpg" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
               
            </div>
        </div>
    </section>
    <!-- certificate area end -->

    <!-- Counter Area -->
    <section class="counter-area section-padding-2 blue-bg-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>our Statistics</h4>
                        <h2>All our Statistics</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="single-counter">
                        <i class="flaticon-shield"></i>
                        <h3 class="count">6589</h3>
                        <h5>Total Transections</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-4 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="single-counter">
                        <i class="flaticon-food-and-restaurant"></i>
                        <h3 class="count">4869</h3>
                        <h5>People Registered</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="single-counter">
                        <i class="flaticon-ui-1"></i>
                        <h3><span class="count">9.2</span>%</h3>
                        <h5>Business Grow</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 wow fadeInUp" data-wow-delay="0.6s">
                    <div class="single-counter">
                        <i class="flaticon-quote"></i>
                        <h3>$<span class="count">18</span>M</h3>
                        <h5>Money Rolling</h5>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /Counter Area -->

    <!--feature Area-->
    <section class="feature-area section-padding-2 blue-bg shaded-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>How to Invest</h4>
                        <h2>Easy way to start Invest</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6">
                    <div class="single-feature-2 bottom-after">
                        <i class="flaticon-projection-screen"></i>
                        <h4>Choose Plan</h4>
                        <p>Identify your financial goals and how soon you'll need the money you plan to invest. Pick the type of investment account you'll use </p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6">
                    <div class="single-feature-2 bottom-before">
                        <i class="flaticon-food-and-restaurant"></i>
                        <h4>Keep Rolling</h4>
                        <p>Certainty listening no no behaviour existence assurance situation is.</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                    <div class="single-feature-2">
                        <i class="flaticon-guarantee"></i>
                        <h4>Enjoy profit</h4>
                        <p>Investing is no different. If you can automate your bills, why not your investments? </p>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/feature Area-->

    <!-- Faq Section -->
    <section class="section-padding blue-bg-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>Any asking</h4>
                        <h2>Freequently Asked Questions</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-30">
                    <img src="/assets/images/feature.png" alt="">
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <div class="faq-contents">
                        <ul class="accordion">
                            <li>
                                <a href="#"><i class="flaticon-save"></i> What is the main idea behind the establishment of Minesrealm.com? Is the company for 'real'? And is there anything that really proves this?</a>
                                <p>Minerealm is a cryptocurrency investment company established in 2019. The wide array of services we offer were specifically designed to assist Bitcoin holders in making good investment decisions. We are a London-based company that possesses a group of investment professionals covering many industries, ranging from engineering to advanced computer technology. Our group of professionals are widely versed in cryptocurrency techniques as well and can expertly answer any question you might have concerning cryptocurrency investment. </p>
                            </li>
                            <li>
                                <a href="#"><i class="flaticon-shield"></i>What condition is required before one can become a member of your investment project? And is membership limited to certain countries?</a>
                                <p>You have to be at least 18 years old before you're allowed to become a member. In addition to this, you must agree to our Terms of Service. Anybody from anywhere in the world can join. Even if you live in the desert, as long as you have an internet connection, you’re free to join.</p>
                            </li>
                            <li>
                                <a href="#"><i class="flaticon-projection-screen"></i> How many accounts are allowed per person? What about sharing my IP address with co-workers or family members?</a>
                                <p> We Allow Multiple Accounts its fine to share your Ip address.</p>
                               
                            </li>
                            <li>
                                <a href="#"><i class="flaticon-food-and-restaurant"></i>How long does it take for my account to start generating interest after I make a deposit?</a>
                                <p>Your account will start receiving interest from the elapse of the first hour after a deposit is made. For example, if your deposit is made at 8:08pm, the first interest will be added at 9:08pm, the second one by 10:08pm and so on.</p>
                            </li>
                            <li>
                                <a href="#"><i class="flaticon-invention"></i> What payment methods can I use to fund my account?</a>
                                <p>We accept BitCoin, PerfectMoney, Payeer, LiteCoin, Ethereum, Bitcoin Cash, Dogecoin and Dash to deposit with us.</p>
                            </li>
                        </ul>
                    </div>
                </div>
                
            </div>
            <div class="row justify-content-center">                
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 centered">
                    <a href="/login/" class="bttn-mid btn-fill">Invest Now</a>
                </div>
            </div>
        </div>
    </section><!-- /Faq Section -->

   

    <!-- Testimonials Area -->
    <section class="section-padding-2 blue-bg-2">
        <div class="container">
            <div class="row">
                <div class="col centered">
                    <div class="section-title cl-white">
                        <h4>Testimonials</h4>
                        <h2>Our Player Reviews</h2>
                    </div>
                </div>
            </div>
            <div class="testimonials owl-carousel">
                <div class="single-testimonial">
                    <div class="testimonial-thumb">
                        <img src="/assets/images/reviewer/1.jpg" alt="">
                    </div>
                    <div class="testimonial-content">
                        <div class="ratings">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                        <p>Wonderful service!! You can invest their plans right now! Site paying me fine. I absolutely trust with this company. Thanks for admin offer us a stable program! You are best!</p>
                        <div class="reviewer-meta">
                            <h5>IRMA BROCKINGTON MADISON</h5>
                            <h6>WISCONSIN</h6>
                        </div>
                    </div>
                </div>
                <div class="single-testimonial">
                    <div class="testimonial-thumb">
                        <img src="/assets/images/reviewer/2.jpg" alt="">
                    </div>
                    <div class="testimonial-content">
                        <div class="ratings">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                        <p>This is a serious paying investment program. and always pays instantly! Sent me payments without any problems. Many thanks to the admin. member</p>
                        <div class="reviewer-meta">
                            <h5>LUCAS FERRANTI</h5>
                            <h6>ORLANDO, FLORIDA</h6>
                        </div>
                    </div>
                </div>
                <div class="single-testimonial">
                    <div class="testimonial-thumb">
                        <img src="/assets/images/reviewer/3.jpg" alt="">
                    </div>
                    <div class="testimonial-content">
                        <div class="ratings">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                        <p>Great service! I have been worried about investing. But when I came here. I don't have to worry anymore. Excellent performance, they continue to make good commitments. Thank you!</p>
                        <div class="reviewer-meta">
                            <h5>LYNN SHEFFIELD WAVERLY</h5>
                            <h6>ILLINOIS</h6>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section><!-- /Testimonials Area -->    
    
    <!-- Newslatter -->
    <section class="section-padding blue-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 centered cl-white">
                    <div class="section-title">
                        <h4>Subscribe</h4>
                        <h2>Don't miss any update</h2>
                    </div>
                    <div class="newslatter">
                        <form action="#">
                            <input type="email" placeholder="Enter your email" required>
                            <button type="submit"><i class="fa fa-paper-plane"></i></button>
                        </form>
                        <p><i class="fa fa-info-circle"></i>We will never send spam</p>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /Newslatter -->

    

    <?php require("footer.php") ?>


    <!-- request -->
    <script src="assets/js/request.js"></script>

    <!-- jQuery (necessary for Bootstrap's Jagit ript plugins) -->
    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/js/jquery-migrate.js"></script>
    <script src="assets/js/jquery-ui.js"></script>

    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>

    <script src="assets/js/magnific-popup.min.js"></script>
    <script src="assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/scrollUp.min.js"></script>

    <script src="assets/js/script.js"></script>
    <script src="assets/js/calculator.js"></script>

</body>

</html>