var plan, amount, period, percentage, interest;
plan = document.querySelector("#plan");
period = document.querySelector("#period");
percentage = document.querySelector("#percentage");
var selected;

const planCfg = {
    b:{
        rate: 2,
        min: 100,
        max:2000
    },
    r:{
        rate:4,
        min:2000,
        max:4000
    },
    pro:{
        rate:6,
        min:4000,
        max:6000
    },
    pre:{
        rate:8,
        min:8000,
        max:1000000
    }
}

// var plans = ["b", "r", "pro", "pre"];

plan.addEventListener('change', function(e) {
    selected = planCfg[e.target.value];
    amount.value = selected.min;
    percentage.value = period.value * selected.rate / 100;

})

var amount = document.querySelector("#amount");
amount.addEventListener('input', calInterest);

function calInterest() {
    var interest = document.querySelector("#interest");
    interest.value = amount.value * period.value / 100;
    percentage.value = period.value * selected.rate / 100;
   
}
period.addEventListener('input', getPercentage);
function getPercentage() {
    percentage.value = period.value * selected.rate / 100;
    interest.value = amount.value * period.value / percentage.value;

}

// alert('hello world');