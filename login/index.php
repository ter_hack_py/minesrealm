<?php 
// Initialize the session
session_start();
 
// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: /profile/");
    // exit;
}else {
    // Include config file
    require_once "../server/db/index.php";
    
    // Define variables and initialize with empty values
    $uid = $pwd = "";
    $uid_err = $pwd_err = "";
    
    // Processing form data when form is submitted
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $uid = $_POST['uid'];
        $pwd = $_POST['pwd'];
    
        if(empty(trim($_POST["uid"]))){
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> <stong>Sorry! </strong>Email field cannot be empty! </div>';
        } 
      
        if(empty(trim($_POST["pwd"]))){
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> <stong>Sorry! </strong>Password field cannot be empty! </div>';;
           
        }
        // Validate credentials
        // if(empty($uid_err) && empty($pwd_err)){
            // Prepare a select statement
            $sql = "SELECT email, pwd FROM users WHERE email = ?";
            
            if($stmt = mysqli_prepare($link, $sql)){

                mysqli_stmt_bind_param($stmt, "s", $param_uid);
               
                $param_uid = $uid;

                if(mysqli_stmt_execute($stmt)){

                    mysqli_stmt_store_result($stmt); 
                    // Check if uid exists, if yes then verify pwd
                    if(mysqli_stmt_num_rows($stmt) == 1){                    
                        // Bind result variables
                        mysqli_stmt_bind_result($stmt, $uid, $hashed_pwd);
                        if(mysqli_stmt_fetch($stmt)){
                            if(password_verify($pwd, $hashed_pwd)){
                             
                                // Store data in session variables
                                $_SESSION["loggedin"] = true;
                                $_SESSION["uid"] = $uid;                            
                                
                                // Redirect user to welcome page
                                header("location: /profile/");
                            } else{
                                // Display an error message if pwd is not valid
                                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button> <stong>Wrong Password! </strong>The Password you entered was not valid ! </div>';
                               
                            }
                        }
                    } else{
                        // Display an error message if uid doesn't exist
                        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button> <stong>Invalid Email! </strong>No Account found with that Email! </div>';
                        
                    }
                } else{
                    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button> <stong>Oops! </strong>Something went wrong, please try again later! </div>';
                    
                }
            }
            
            // Close statement
            mysqli_stmt_close($stmt);
        // }
        
        // Close connection
        mysqli_close($link);
    }
}
 

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=1024">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        
    <link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../assets/images/favicon.ico" type="image/x-icon">

    <title>Mines Realm - Login</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="../assets/css/magnific-popup.css" rel="stylesheet">
    <link href="../assets/css/jquery-ui.css" rel="stylesheet">


    <link href="../assets/css/animate.html" rel="stylesheet">
    <link href="../assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="../assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <!-- Preloader -->
    <!-- <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div> -->
    <!--/Preloader -->

   
    <?php require_once("../header.php") ?>
    
    <!--Login Section -->
    <section class="section-padding blue-bg shaded-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                    <div class="account-form cl-white">
                        <div class="title">
                            <h3>Login to your account</h3>
                        </div>
                        <div class="via-login">
                            <a href="javascript:void(0)" class="facebook-bg"><i class="fa fa-facebook"></i></a>
                            <a href="javascript:void(0)" class="google-plus-bg"><i class="fa fa-google"></i></a>
                            <div class="g-signin2" data-onsuccess="onSignIn"></div>
                            <a href="javascript:void(0)" class="linkedin-bg"><i class="fa fa-linkedin"></i></a>
                            <a href="javascript:void(0)" class="instagram-bg"><i class="fa fa-instagram"></i></a>
                            <a href="javascript:void(0)" class="twitter-bg"><i class="fa fa-twitter"></i></a>
                        </div>
                        <form action="#" method="post">
                            <div class="row">
                                <div class="col-xl-12">
                                    <p>Or with your Email and password</p>
                                </div>
                                <div class="col-xl-12">
                                    <input type="email" name="uid" placeholder="Email" value="<?php if (isset($_SESSION['email'])) {  
                                        echo $_SESSION['email'];
                                    } ?>">
                                </div>
                                <div class="col-xl-12">
                                    <input type="password" name="pwd" placeholder="Password" value="<?php if (isset($_SESSION['pwd'])) {  
                                        echo $_SESSION['pwd'];
                                    } ?>">
                                </div>
                                
                                <div class="col-xl-12">
                                    <button name="login" type="submit" class="bttn-small btn-fill w-100">Login Account</button>
                                </div>
                                <div class="col-xl-12">
                                    <p><a href="/reset_pass/">Forgot password?</a> <a href="/register/">Signup</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/Login Section-->


    <?php require_once("../footer.php") ?>
    <!--/Footer Area-->




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../assets/js/jquery-3.2.1.min.js"></script>
    <script src="../assets/js/jquery-migrate.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>

    <script src="../assets/js/popper.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/owl.carousel.min.js"></script>

    <script src="../assets/js/magnific-popup.min.js"></script>
    <script src="../assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="../assets/js/isotope.pkgd.min.js"></script>
    
    <script src="../assets/js/waypoints.min.js"></script>
    <script src="../assets/js/jquery.counterup.min.js"></script>
    <script src="../assets/js/wow.min.js"></script>
    <script src="../assets/js/scrollUp.min.js"></script>

    <script src="../assets/js/script.js"></script>

    <!-- Google Login script -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
</body>

</html>