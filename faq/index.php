<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=1024">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        
    <link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../assets/images/favicon.ico" type="image/x-icon">

    <title>Minesrealm - FAQ</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="../assets/css/magnific-popup.css" rel="stylesheet">
    <link href="../assets/css/jquery-ui.css" rel="stylesheet">


    <link href="../assets/css/animate.html" rel="stylesheet">
    <link href="../assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="../assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <!-- Preloader -->
    <!-- <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div> -->
    <!--/Preloader -->

    <?php require_once('../header.php') ?>
    
    
    <!-- Faq Section -->
    <section class="section-padding blue-bg-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>Any asking</h4>
                        <h2>Freequently Asked Questions</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                    <div class="faq-contents">
                        <ul class="accordion">
                            <li>
                                <a href="#"><i class="flaticon-save"></i> What is the main idea behind the establishment of Minesrealm.com? Is the company for 'real'? And is there anything that really proves this?</a>
                                <p>Minerealm is a cryptocurrency investment company established in 2019. The wide array of services we offer were specifically designed to assist Bitcoin holders in making good investment decisions. We are a London-based company that possesses a group of investment professionals covering many industries, ranging from engineering to advanced computer technology. Our group of professionals are widely versed in cryptocurrency techniques as well and can expertly answer any question you might have concerning cryptocurrency investment. </p>
                            </li>
                            <li>
                                <a href="#"><i class="flaticon-shield"></i>What condition is required before one can become a member of your investment project? And is membership limited to certain countries?</a>
                                <p>You have to be at least 18 years old before you're allowed to become a member. In addition to this, you must agree to our Terms of Service. Anybody from anywhere in the world can join. Even if you live in the desert, as long as you have an internet connection, you’re free to join.</p>
                            </li>
                            <li>
                                <a href="#"><i class="flaticon-projection-screen"></i> How many accounts are allowed per person? What about sharing my IP address with co-workers or family members?</a>
                                <p> We Allow Multiple Accounts its fine to share your Ip address.</p>
                               
                            </li>
                            <li>
                                <a href="#"><i class="flaticon-food-and-restaurant"></i>How long does it take for my account to start generating interest after I make a deposit?</a>
                                <p>Your account will start receiving interest from the elapse of the first hour after a deposit is made. For example, if your deposit is made at 8:08pm, the first interest will be added at 9:08pm, the second one by 10:08pm and so on.</p>
                            </li>
                            <li>
                                <a href="#"><i class="flaticon-invention"></i> What payment methods can I use to fund my account?</a>
                                <p>We accept BitCoin, PerfectMoney, Payeer, LiteCoin, Ethereum, Bitcoin Cash, Dogecoin and Dash to deposit with us.</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 mb-30">
                    <img src="../assets/images/feature.png" alt="">
                </div>
            </div>
            <div class="row justify-content-center">                
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 centered">
                    <a href="../login/" class="bttn-mid btn-fill">Invest Now</a>
                </div>
            </div>
        </div>
    </section><!-- /Faq Section -->

    <!--Footer Area -->
    <?php require_once('../footer.php') ?>
   <!--/Footer Area-->




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../assets/js/jquery-3.2.1.min.js"></script>
    <script src="../assets/js/jquery-migrate.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>

    <script src="../assets/js/popper.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/owl.carousel.min.js"></script>

    <script src="../assets/js/magnific-popup.min.js"></script>
    <script src="../assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="../assets/js/isotope.pkgd.min.js"></script>
    
    <script src="../assets/js/waypoints.min.js"></script>
    <script src="../assets/js/jquery.counterup.min.js"></script>
    <script src="../assets/js/wow.min.js"></script>
    <script src="../assets/js/scrollUp.min.js"></script>

    <script src="../assets/js/script.js"></script>
</body>

</html>