<?php
// Processing form data when form is submitted
if(isset($_GET["email"]) && !empty($_GET["email"])){

    // define('DB_SERVER', 'localhost');
    // define('DB_USERNAME', 'root');
    // define('DB_PASSWORD', '');
    // define('DB_NAME', 'minesrea_server');

    // live server
    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'minesrea_admin');
    define('DB_PASSWORD', '%G^q;~_E8E7x');
    define('DB_NAME', 'minesrea_server'); 
    /* Attempt to connect to MySQL database */
    
    $link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    // Check connection
    if($link === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());

    }
    // Get hidden input value
    $email = $_GET["email"];
    $pin = rand('26545', '99722');

    $sql = "SELECT email FROM users WHERE email = ?";
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "s", $param_email);
        
        // Set parameters
        $param_email = $email;
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            /* store result */
            mysqli_stmt_store_result($stmt);
            
            if(mysqli_stmt_num_rows($stmt) == 1){
                $username_err = "This username is already taken.";
                // Update recovery pin
                $sql = "UPDATE users SET pin=? WHERE email=?";
         
                if($stmt = mysqli_prepare($link, $sql)){
                    // Bind variables to the prepared statement as parameters
                    mysqli_stmt_bind_param($stmt, "ss", $param_pin, $param_email);
                    
                    // Set parameters
                    $param_email = $email;
                    $param_pin = $pin;
                
                    // Attempt to execute the prepared statement
                    if(mysqli_stmt_execute($stmt)){
                        // send mail
                        $to = $email;
                        $subject = 'Reset password';
                        $from = 'admin@minesrealm.com';
                    
                        // start
                        // To send HTML mail, the Content-type header must be set
                        $headers  = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        
                        // Create email headers
                        $headers .= 'From: '.$from."\r\n".
                            'Reply-To: '.$from."\r\n" .
                            'X-Mailer: PHP/' . phpversion();
                        
                        // Compose a simple HTML email message
                        $message = '<html><body>';
                        $message .= '<h1 style="color:#f40;"> ' . 'Hi' . '</h1>';
                        $message .= '<p style="color:#080;font-size:18px;">Your reset code</p>';
                        $message .= '<p style="color:#080;font-size:18px;">'.$pin.'</p>';
        
                        $message .= '<p><img src="https://minesrealm.com/assets/images/1.jpeg" /></p>';
        
                        $message .= '</body></html>';
                        // stop
                        
                        // Sending email
                        if(mail($to, $subject, $message, $headers)){
                            // header("location: index.php");
                            echo '<br><br>' .  '<div id="success-alert" class="alert alert-success fade-out">
                                <strong>Success! </strong><a href="#" class="alert-link">' .'Reset code sent to, <strong>'.$email.'</strong>' .'</a>.
                            </div>';
                        } else{
                            echo '<br><br>' . '<div class="alert alert-warning float-lg-right">
                                <strong>Error!</strong> Failed to send <a href="#" class="alert-link">' . 'Something went wrong, please try again later' . '</a>' .
                            '</div>';
                        }
                       
                    } else{

                        echo "Something went wrong. Please try again later.";
                    }
                }
                

            } else{
                echo '<div class="alert alert-warning">No Account found with this email!</div>';
            }
        } 

        // Close statement
        mysqli_stmt_close($stmt);
    }

} 
?>