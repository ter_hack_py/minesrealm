<?php

if(!isset($_GET['i_name']) || empty($_GET['amount']) || empty($_GET['category'])) {
    echo '<div id="success-alert" class="alert alert-warning fadeout">
        <a href="#" class="alert-link">' .' <strong>' .'No please'. '</strong>'. ' Fields cannot be empty' .'</a>.
    </div>';
}else{
    // define('DB_SERVER', 'localhost');
    // define('DB_USERNAME', 'root');
    // define('DB_PASSWORD', '');
    // define('DB_NAME', 'minerealm');

    // live server
    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'minesrea_admin');
    define('DB_PASSWORD', '%G^q;~_E8E7x');
    define('DB_NAME', 'minesrea_server');   
    
    /* Attempt to connect to MySQL database */
    $link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    
    // Check connection
    if($link === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());
    }
    $deposit = '';
    $withdrawal = '';
    $type = '';
    if (!isset($_GET['i_name']) || empty($_GET['amount'])) {
        echo '<div id="success-alert" class="alert alert-warning fadeout">
                <a href="#" class="alert-link">' .' <strong>' .'Error'. '</strong>'. ' Empty input values ' . '<br>'. '<strong><i class="fa fa-lightbulb-o"></i> Tips: </strong> <i>Fill all spaces</i>' .'</a>.
            </div>';
    }
    $amount = $_GET['amount'];
    filter_var($amount,FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
    
    if ($_GET['category'] == 1) {
        $deposit = $amount;
        $type = 1;
    }elseif($_GET['category'] == 2){
        $withdrawal = $amount;
        $type = 2;
    }
    $i_name = trim($_GET['i_name']);
    $i_id = rand('12345', '98765');
    
    // $uid = md5(rand(0,1000) );
    $check_user_query = "SELECT * FROM investors WHERE i_name = '$i_name' AND deposit = '$deposit' AND withdrawal = '$withdrawal'";
    $result = mysqli_query($link, $check_user_query);
    $resultCheck = mysqli_num_rows($result);
    
    if ($resultCheck > 0) {
        echo '<div id="success-alert" class="alert alert-warning fadeout">
                <a href="#" class="alert-link">' .' <strong>' .$i_name. '</strong>'. ' Already taken ' .'with the same investment plan and category '. '<br>'. '<strong><i class="fa fa-lightbulb-o"></i> Tips: </strong> <i>Try changing the Name, amount or category</i>' .'</a>.
            </div>';
        // header("Location:index.php");
    }else {
        $insert_sql = "INSERT INTO investors (i_name, investor_id, deposit, withdrawal, i_type) VALUES (?, ?, ?, ?, ?)";
        if($stmt = mysqli_prepare($link, $insert_sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ssssi", $param_i_name, $param_i_id, $param_deposit, $param_withdrwal, $param_type);
            // Set parameters
            $param_i_name = $i_name;
            $param_i_id = $i_id;
            $param_deposit = $deposit;
            $param_withdrwal = $withdrawal;
            $param_type = $type;
            // $param_amount = $amount;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                echo '<div id="success-alert" class="alert alert-success fade-out">
                    <strong>Success! </strong><a href="#" class="alert-link">' .'Investor added, <strong>'.$i_name.'</strong>' .'</a>.
                </div>';
            }else {
                echo '<div class="alert alert-warning float-lg-right">
                        <strong>Error!</strong> Technical error <a href="#" class="alert-link">' . 'Something went wrong, please try again later' . '</a>' .
                '</div>';
            }
        }
        // Close statement
        mysqli_stmt_close($stmt);
        // Close connection
        mysqli_close($link); 
    }      
        

        
}

?>