
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        
    <link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../assets/images/favicon.ico" type="image/x-icon">

    <title>Mines Realm - Reset password</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="../assets/css/magnific-popup.css" rel="stylesheet">
    <link href="../assets/css/jquery-ui.css" rel="stylesheet">


    <link href="../assets/css/animate.html" rel="stylesheet">
    <link href="../assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="../assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <!-- Preloader -->
    <!-- <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div> -->
    <!--/Preloader -->

   
    <?php require_once("../header.php") ?>
    
    <!--Login Section -->
    <section class="section-padding blue-bg shaded-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                    <div class="account-form cl-white">
                        <div class="title">
                            <h3>Reset Password</h3>
                            <p>Enter your Email to reset your password</p>
                            <p id='resp1'></p>
                        </div>
                        <form action="#" method="post">
                            <div class="row">
                                <div class="col-xl-12">
                                </div>
                                <div class="col-xl-12">
                                    <input type="email" name="uid" placeholder="Email" value="" id="oldemail">
                                </div>
                                <div class="col-xl-12">
                                    <button id="continueReset" type="button" data-toggle="modal" data-target="#activate-modal" class="bttn-small btn-fill w-100">Continue</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/Login Section-->
    <!-- activate modal -->
    

<div class="modal fade" id="activate-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Activate Account</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
            <div class="form-group">
                <label for="recipient-name" class="col-form-label" id="resp"></label>
                <!-- <input type="email" class="form-control" id="email"> -->
            </div>
            <div class="form-group">
                <label for="recipient-name" class="col-form-label">Email</label>
                <input type="email" class="form-control" id="email">
            </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Enter your pin</label>
            <input type="" class="form-control" id="pin">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">New password</label>
            <input type="text" class="form-control" id="newpwd">
          </div>
        </form>
      </div>
      <div class="modal-footer">
            <!-- <p id="resp">hello</p> -->
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> -->
        <button id="reset_now" type="button" class="bttn-small btn-fill w-100"><span id="btn-show">Reset Now</span>
            <div id='spinner' style="display:none; text-align:center; margin-left:200px" class="spinner-border text-light" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </button>
      </div>
    </div>
  </div>
</div>

    <?php require_once("../footer.php") ?>
    <!--/Footer Area-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../assets/js/jquery-3.2.1.min.js"></script>
    <script src="../assets/js/jquery-migrate.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>

    <script src="../assets/js/popper.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/owl.carousel.min.js"></script>

    <script src="../assets/js/magnific-popup.min.js"></script>
    <script src="../assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="../assets/js/isotope.pkgd.min.js"></script>
    
    <script src="../assets/js/waypoints.min.js"></script>
    <script src="../assets/js/jquery.counterup.min.js"></script>
    <script src="../assets/js/wow.min.js"></script>
    <script src="../assets/js/scrollUp.min.js"></script>

    <script src="../assets/js/script.js"></script>

    <!-- Google Login script -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <script>
        var ajaxRequest = new XMLHttpRequest(); 
        var continueReset = document.querySelector("#continueReset");
        continueReset.addEventListener('click', continueNow);
        function continueNow(e) {
            e.preventDefault();
            document.getElementById('email').value = oldemail.value;
            ajaxRequest.onreadystatechange = function(){
                var resp = document.getElementById('resp');
                if (ajaxRequest.readyState < 4) {
                    // document.getElementById('btn-show').style.display='none';
                    // document.getElementById('spinner').style.display='block';

                }else if (ajaxRequest.readyState == 4) {
                    
                    // document.getElementById('overlay').style.display='none';
                
                    resp.innerHTML = ajaxRequest.responseText;
                }
            
            }
           
            const _withdrawal = document.getElementById('withdrawal');

            var queryString = "?email=" + oldemail.value;
            // queryString += "&pin=" + pin.value;
            // queryString += "&pwd=" + newpwd.value;
           
            ajaxRequest.open("GET", "/server/request/reset_password.php" + queryString, true);
            ajaxRequest.send(null);
        }

        // var sendMail, msg, r_name, r_email;
        // var oldemail = document.querySelector("#oldemail").value;
        
        // continue.addEventListener('click', getEmail);
        // function getEmail() {
        //     email.value = oldmail.value;
        // }


        reset = document.querySelector("#reset_now");
        reset.addEventListener('click', resetNow);
        
        function resetNow(e) {
            e.preventDefault();
            ajaxRequest.onreadystatechange = function(){
                var resp = document.getElementById('resp');
                if (ajaxRequest.readyState < 4) {
                    document.getElementById('btn-show').style.display='none';
                    document.getElementById('spinner').style.display='block';

                }else if (ajaxRequest.readyState == 4) {
                    
                    // document.getElementById('overlay').style.display='none';
                    document.getElementById('spinner').style.display='none';
                    // document.getElementById('btn-show').style.display='block';

                    
                    resp.innerHTML = ajaxRequest.responseText;
                }
            
            }
           
            // const _withdrawal = document.getElementById('withdrawal');

            var queryString = "?email=" + email.value;
            queryString += "&pin=" + pin.value;
            queryString += "&pwd=" + newpwd.value;
           
            ajaxRequest.open("GET", "/server/request/activate_pass.php" + queryString, true);
            ajaxRequest.send(null);
        }
    </script>
</body>

</html>