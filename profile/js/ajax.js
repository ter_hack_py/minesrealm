let ajaxRequest; 
        
    try {
        
        ajaxRequest = new XMLHttpRequest();
    }catch (e) {
        
        try {
            ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try{
                ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e){
                
                alert("Your browser broke!");
                
            }
        }
    }
function regParticipant() {
    // let ajaxRequest; 
    
     // var resp = response.reference;
    
    ajaxRequest.onreadystatechange = function(){
      
        var display = document.getElementById("display");
        var showload = document.getElementById('showload');
        if (ajaxRequest.readyState < 4) {
            showload.style.display="block";
        }else if (ajaxRequest.readyState == 4) {
            // showload.style.display="none";
            display.innerHTML = ajaxRequest.responseText;
        }
    
    }
   
    var regBy = document.getElementById("regBy");
    var queryString = "?participant_email=" + participant_email.value;
    queryString +=  "&participant_name=" + participant_name.value;
    queryString += "&participant_phone=" + participant_phone.value;
    queryString += "&participant_class=" + participant_class.value;
    queryString += "&regBy=" + regBy.value;

    // var queryString = "?" + email;
    // ajaxRequest.open("GET", console.log(queryString));
    ajaxRequest.open("GET", "../main/api/register.php" + queryString, true);
    ajaxRequest.send(null);
}
function regUser() {
        
        ajaxRequest.onreadystatechange = function(){
            // var resp = response.reference;
            const invite_name = document.getElementById("invite_name").value;
            var email = document.getElementById("email").value;
            // let name = document.getElementById("name").value;
            var pwd = document.getElementById("pwd").value;

            // var msg = document.create
            var ajaxDisplay = document.getElementById('ajaxDisplay');
            var loader = document.getElementById("loading");
            if (ajaxRequest.readyState < 4) {
                loader.style.display = "block";
                ajaxDisplay.innerHTML = "Please wait......";
            }else if(ajaxRequest.readyState == 4){
                loader.style.display = "none";
                ajaxDisplay.innerHTML = ajaxRequest.responseText;
            }
        }

        var queryString = "?email=" + email.value;
        // queryString +=  "&name=" + name.value;
        queryString +=  "&pwd=" + pwd.value;
        queryString += "&invite_name=" + invite_name.value;
        // var queryString = "?" + email;
        // ajaxRequest.open("GET", console.log(queryString));
        ajaxRequest.open("GET", "../main/api/invite.php" + queryString, true);
        ajaxRequest.send(null); 
        
}

const submitBtn = document.getElementById("submit-btn")
// console.log(submitBtn)
submitBtn.addEventListener("click", submit)

function submit(){
    var topic = document.getElementById("topic");
    var user = document.getElementById("user");
    var comment = document.getElementById("comment");
    // var row = document.getElementById("row");
    var row_r = document.getElementById("row-r");

    var speaker = document.getElementById("speaker");
    var loadEffect = document.getElementById("loadEffect");

    ajaxRequest.onreadystatechange = function(){
        if (ajaxRequest.readyState < 4) {
            loadEffect.style.display="block";

        }else if (ajaxRequest.readyState == 4) {
            loadEffect.style.display="none";
            row_r.innerHTML = ajaxRequest.responseText;

            // row.innerHTML = ajaxRequest.responseText;
        }
    }

    // var regBy = document.getElementById("regBy");
    var queryString = "?topic=" + topic.value;
    queryString +=  "&user=" + user.value;
    queryString += "&comment=" + comment.value;
    queryString += "&speaker=" + speaker.value;
        // ajaxRequest.open("GET", console.log(queryString));

    ajaxRequest.open("GET", "../main/api/question.php" + queryString, true);
    ajaxRequest.send(null);
}