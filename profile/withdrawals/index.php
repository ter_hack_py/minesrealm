<?php
session_start();
if (!isset($_SESSION["uid"]) || $_SESSION['loggedin'] === false) {
    header("Location: /login/");
}else {
    if (isset($_SESSION['uid'])) {
        $email = $_SESSION['uid'];
        require_once("../db.php");
        $sql = "SELECT * FROM users  WHERE email = '$email'";
        $result = mysqli_query($link, $sql);
        if (mysqli_num_rows($result) > 0 )  {
            while ($row = mysqli_fetch_array($result)) {
                $username = $row['username'];
                $full_name = $row['full_name'];
                $email = $row["email"];
                $phone = $row['phone'];
                $bitcoin = $row['payment'];
                $id = rand("12344", "23393");
                $reg_at =  $row["reg_at"];
            }
        }
    }
}
if (isset($_SESSION['uid']) && $_SESSION['uid'] !== 'admin@minesrealm.com') {

    header('Location: /login/');
 }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=1024"> -->

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        
    <link rel="shortcut icon" href="/assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">

    <title>Withdrawal request</title>

    <!-- Bootstrap -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/magnific-popup.css" rel="stylesheet">
    <link href="/assets/css/jquery-ui.css" rel="stylesheet">


    <link href="/assets/css/animate.html" rel="stylesheet">
    <link href="/assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="/assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        #overlay {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.5);
        z-index: 2;
        cursor: pointer;
        }

        #text{
        position: absolute;
        top: 50%;
        left: 50%;
        font-size: 20px;
        color: white;
        transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
        }
    </style>

</head>
<body>

    <!-- Preloader -->
    <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div><!--/Preloader -->
    <?php require_once('../../header.php') ?>
    <!-- table -->
    <section class="section-padding blue-bg shaded-bg" style="z-index: 99999999999;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>Users Withdrawal Table</h4>
                        <h2>Withdrawals</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-sm-12 mb-30">
                    <div class="winner-list table-responsive">
                        <table class="table table-dark table-hover table-striped table-borderless">
                            <thead>
                              <tr>
                                <th scope="col">Bitcoin Address</th>
                                <th scope="col">Email</th>
                                <th scope="col">Date</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Date Approved</th>
                                <th scope="col">Status</th>
                                
                              </tr>
                            </thead>
                            <tbody>
                            <?php 
                           
                            
                            /* Attempt to connect to MySQL database */
                            $link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
                            
                            // Check connection
                            if($link === false){
                                die("ERROR: Could not connect. " . mysqli_connect_error());
                            }
                            $sql = "SELECT * FROM withdrawal_request";
                            $result = mysqli_query($link, $sql);
                            if (mysqli_num_rows($result) > 0 )  {
                                while ($row = mysqli_fetch_array($result)) {
                                    $email = $row['email'];
                                    $approved_at = '';
                                    if ($row['isApproved'] != 0){
                                        $approved_at = $row['approved_at'];
                                    }else {
                                        $approved_at = 'Not yet Approved';
                                    }
                                    // $getUser = "SELECT full_name FROM users WHERE email = '$email'";
                                    // $userResult = mysqli_query($link, $getUser);
                                    // if (mysqli_num_rows($userResult) > 0 ) {
                                    //     while ($row = mysqli_fetch_array($userResult)) {
                                    //         $name = $row['full_name'];
                                    //         $balance = $row['deposit'];
                                    //     }
                                    // }

                                    ?>
                                    <tr>
                                        <th scope="row"><?php echo $row['bitcoin_address']?></th>
                                        <td class="cl-yellow"><?php echo $row['email'] ?></td>
                                        <td class="cl-mint"><?php echo $row['requested_at'] ?></td>
                                        <td class="cl-mint"><?php echo $row['amount'] ?></td>
                                        <td class="cl-mint"><?php echo $approved_at ?></td>

                                        <?php if ($row['isApproved'] == 0) {
                                            $status = 'Pending';
                                            }else {
                                                $status = 'Approved';
                                            } ?>

                                            <!-- <div class="dropdown"> -->
                                                
                                                <td><a href="approve.php?user=<?php echo $row['email'] ?>&amount=<?php echo $row['amount'] ?>" class="bttn-small btn-fill"  ><?php echo $status ?></a></td>

                                                
                                       
                                        
                                    </tr>
                                   
                                  <!-- stop -->
                                  <?php
                                }
                            }else {
                                echo 'No record found';
                            }

                            ?>
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/Winner List-->
    <?php require_once('../../footer.php') ?>
    <script>
        // function off() {
        //     document.getElementById("overlay").style.display = "none";
        // }
        // let ajaxRequest; 
        // try {
        //     ajaxRequest = new XMLHttpRequest();
        // }catch (e) {
            
        //     try {
        //         ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
        //     }catch (e) {
        //         try{
        //             ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
        //         }catch (e){
                    
        //             alert("Your browser broke!");
                    
        //         }
        //     }
        // }
       
        // const addAmount = document.getElementById('add_amount');
        // const amount = document.getElementById('amount');
        
        // addAmount.addEventListener('click', submitAmount);
        // amount.addEventListener('input', displaySubmit);
        // if (amount.value = "") {
        //     addAmount.style.display = 'none';
        // }
        // function displaySubmit() {
        //     addAmount.style.display = 'block';
        // }
        // function submitAmount(e) {
        //     var deposit = document.getElementById('deposit').innerHTML;
        //     var sid = document.getElementById('sid');
        //     e.preventDefault();
        //     ajaxRequest.onreadystatechange = function(){
        //         var resp = document.getElementById('resp');
        //         if (ajaxRequest.readyState < 4) {
                  
        //             document.getElementById('overlay').style.display='block';
        //         }else if (ajaxRequest.readyState == 4) {
        //             // addUser.style.display="block";
        //             document.getElementById('overlay').style.display='none';
        //             // console.log(queryString);
        //             // showload.style.display="none";
        //             resp.innerHTML = ajaxRequest.responseText;
        //         }
            
        //     }

        //     // var queryString = "?amount=" + amount.value;
        //     // queryString +=  "&sid=" + sid.value;
        //     // queryString += "&sid=" + sid.value;
        //     console.log(sid.value + amount.value);
        //     // ajaxRequest.open("GET", "/server/request/index.php" + queryString, true);
        //     // ajaxRequest.send(null);
        // }
    </script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/assets/js/jquery-3.2.1.min.js"></script>
    <script src="/assets/js/jquery-migrate.js"></script>
    <script src="/assets/js/jquery-ui.js"></script>

    <script src="/assets/js/popper.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/owl.carousel.min.js"></script>

    <script src="/assets/js/magnific-popup.min.js"></script>
    <script src="/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="/assets/js/isotope.pkgd.min.js"></script>
    
    <script src="/assets/js/waypoints.min.js"></script>
    <script src="/assets/js/jquery.counterup.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/scrollUp.min.js"></script>

    <script src="/assets/js/script.js"></script>
</body>
</html>