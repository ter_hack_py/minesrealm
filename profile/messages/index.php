<?php 
// define('DB_SERVER', 'localhost');
// define('DB_USERNAME', 'root');
// define('DB_PASSWORD', '');
// define('DB_NAME', 'minerealm');

// live server
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'minesrea_admin');
define('DB_PASSWORD', '%G^q;~_E8E7x');
define('DB_NAME', 'minesrea_server');   

/* Attempt to connect to MySQL database */
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        
    <link rel="shortcut icon" href="/assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">

    <title>Messages</title>

    <!-- Bootstrap -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/magnific-popup.css" rel="stylesheet">
    <link href="/assets/css/jquery-ui.css" rel="stylesheet">


    <link href="/assets/css/animate.html" rel="stylesheet">
    <link href="/assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="/assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <!-- Preloader -->
    <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div><!--/Preloader -->

    <!--Header Top-->
    <div class="header-top blue-bg-2">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                    <div class="single-header-top">
                        <p>
                            <span>Welcome to MinesRealm!</span>
                            <!-- <span><i class="fa fa-phone"></i> (+44) 959-595-959</span> -->
                        </p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                    <div class="single-header-top last">
                        <div class="account-menu">
                            <select name="country">
                                <option value="">Eng</option>
                                <option value="">Fra</option>
                                <option value="">Spn</option>
                                <option value="">Ger</option>
                            </select>
                            <ul>
                                <li><a href="">Support</a></li>
                                <li><a href="../logout.php"><i class="fa fa-key"></i>Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div><!--/Header Top-->
    <header class="header-area blue-bg">
        <nav class="navbar navbar-expand-lg main-menu">
            <div class="container">
                <!-- <a class="navbar-brand" href="index-2.html"><img src="//assets/images/logo-2.png" class="d-inline-block align-top" alt=""></a> -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="menu-toggle"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <!-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="blog.html" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Home</a> -->
                            <!-- <ul class="dropdown-menu"> -->
                                <!-- <li><a class="nav-item" href="index.php">Home</a></li> -->
                                
                            <!-- </ul> -->
                        <!-- </li> -->
                        <li class="nav-item"><a class="nav-link" href="/">Home</a></li>
                        
                        <li class="nav-item"><a class="nav-link" href="/interest/">Interest</a></li>
                        <li class="nav-item"><a class="nav-link" href="/about/">About</a></li>
                        <li class="nav-item"><a class="nav-link" href="/faq/">FAQ</a></li>
                        <li class="nav-item"><a class="nav-link" href="/testimonials/">Testimonials</a></li>
            <!-- 
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="about.html">About</a></li>
                                <li><a class="dropdown-item" href="contact.html">Contact</a></li>
                                <li><a class="dropdown-item" href="faq.html">FAQ page</a></li>
                                <li><a class="dropdown-item" href="testimonials.html">Success Stories</a></li>
                                <li><a class="dropdown-item" href="promotions.html">Promotions</a></li>
                                <li><a class="dropdown-item" href="how-to-invest.html">How to invest</a></li>
                                <li><a class="dropdown-item" href="affiliate.html">Affiliate</a></li>
                                <li><a class="dropdown-item" href="login.html">Login</a></li>
                                <li><a class="dropdown-item" href="404.html">Error page</a></li>
                                <li><a class="dropdown-item" href="privacy.html">Privacy</a></li>
                                <li><a class="dropdown-item" href="tos.html">Terms and Services</a></li>
                            </ul>
                        </li> -->
                        
                    
                    </ul>
                    <div class="header-btn justify-content-end">
                        <a href="../logout.php" class="bttn-small btn-fill"><i style="font-size:20px" class="fa fa-key"></i></a>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <!--/Header Area-->
    
    <!--Blog Area-->
    <section class="blog-area section-padding-2 blue-bg shaded-bg" style="height:1000px">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="row justify-content-center">
                    <?php  
                            $sql = "SELECT * FROM messages";
                            $result = mysqli_query($link, $sql);
                            if (mysqli_num_rows($result) > 0 )  {
                                while ($row = mysqli_fetch_array($result)) { ?>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                           
                            <div class="single-blog">
                                
                                <div class="single-blog-content">
                                    <div class="blog-meta">
                                        <span><a href="#"><i class="flaticon-marsupial"></i> <?php echo $row['r_name'] ?></a></span>
                                        <span><a href="#"><i class="flaticon-category"></i> <?php $sent_at['sent_at'] = $sent_at; echo $sent_at = date("F j, Y, g:i a");?></a></span>
                                        <span><a href="#"><i class="fa fa-envelope"></i> <?php echo $row['r_email'] ?></a></span>
                                    </div>
                                    <h3><a href=""><?php echo $row['r_msg'] ?></p>
                                </div>
                            </div>
                            
                        </div>
                        <?php
                                }
                            }else {
                                echo '<div class="col centered">
                                <h5>No record found</h5>
                            </div>';
                            }
                            ?>
                                     
                    </div>
                </div>
                
            </div>
        </div>
    </section><!--/Blog Area-->
    <?php require_once('../../footer.php') ?>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/assets/js/jquery-3.2.1.min.js"></script>
    <script src="/assets/js/jquery-migrate.js"></script>
    <script src="/assets/js/jquery-ui.js"></script>

    <script src="/assets/js/popper.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/owl.carousel.min.js"></script>

    <script src="/assets/js/magnific-popup.min.js"></script>
    <script src="/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="/assets/js/isotope.pkgd.min.js"></script>
    
    <script src="/assets/js/waypoints.min.js"></script>
    <script src="/assets/js/jquery.counterup.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/scrollUp.min.js"></script>

    <script src="/assets/js/script.js"></script>
</body>

<!-- Mirrored from weblos.net/HTML/hyipzon/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 May 2020 13:08:50 GMT -->
</html>