<?php
session_start();
if (!isset($_SESSION["uid"]) || $_SESSION['loggedin'] === false) {
    header("Location: /login/");
}else {
    if (isset($_SESSION['uid'])) {
        $email = $_SESSION['uid'];
        require_once("../db.php");
        $sql = "SELECT * FROM users  WHERE email = '$email'";
        $result = mysqli_query($link, $sql);
        if (mysqli_num_rows($result) > 0 )  {
            while ($row = mysqli_fetch_array($result)) {
                $username = $row['username'];
                $full_name = $row['full_name'];
                $email = $row["email"];
                $phone = $row['phone'];
                $bitcoin = $row['payment'];
                $id = rand("12344", "23393");
                $reg_at =  $row["reg_at"];
            }
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        
    <link rel="shortcut icon" href="/assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">

    <title>Users</title>

    <!-- Bootstrap -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/magnific-popup.css" rel="stylesheet">
    <link href="/assets/css/jquery-ui.css" rel="stylesheet">


    <link href="/assets/css/animate.html" rel="stylesheet">
    <link href="/assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="/assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <!-- Preloader -->
    <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div><!--/Preloader -->

    <!--Header Top-->
        <!--Header Top-->
        <?php require_once('../../header.php') ?>
    <!--/Header Area-->
    
    <!--Promotions Section-->
    <section class="section-padding-2 blue-bg shaded-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>All Users</h4>
                        <h2>Users</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php 
                $sql = "SELECT * FROM users";
                $result = mysqli_query($link, $sql);
                if (mysqli_num_rows($result) > 0 )  {
                    while ($row = mysqli_fetch_array($result)) {
                        $username = $row['username'];
                        $full_name = $row['full_name'];
                        $email = $row["email"];
                        $phone = $row['phone'];
                        $bitcoin = $row['payment'];
                        $id = rand("12344", "23393");
                        $reg_at =  $row["reg_at"];
                        ?>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="single-jackpot">
                                <div class="jackpot-thumb">
                                    <img src="/assets/images/profile.jpg" alt="">
                                </div>
                                <div class="jackpot-content">
                                    <h3>Name: <?php echo $full_name ?></h3>
                                    <h6>Email: <?php echo $email ?></h6>
                                    <h6>Phone: <?php echo $phone ?></h6>
                                    <h6>ID: <?php echo $id ?></h6>
                                    <h6>Registered on: <?php echo $reg_at ?></h6>
                                    
                                    <a href="tel: <?php echo '+234' . $phone ?>" class="bttn-small btn-fill">Contact user</a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                 ?>
                
            </div>
        </div>
    </section><!--/Promotions Section-->

    <!--Footer Area -->
   

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="copy-text">
                        <p>Copyright &copy; 2020. All rights reserved</p>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="copy-nav">
                        <ul>
                            <li><a href="#">Privacy</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Terms & service</a></li>
                            <li><a href="#">Conditions</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/Footer Area-->




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/assets/js/jquery-3.2.1.min.js"></script>
    <script src="/assets/js/jquery-migrate.js"></script>
    <script src="/assets/js/jquery-ui.js"></script>

    <script src="/assets/js/popper.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/owl.carousel.min.js"></script>

    <script src="/assets/js/magnific-popup.min.js"></script>
    <script src="/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="/assets/js/isotope.pkgd.min.js"></script>
    
    <script src="/assets/js/waypoints.min.js"></script>
    <script src="/assets/js/jquery.counterup.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/scrollUp.min.js"></script>

    <script src="/assets/js/script.js"></script>
</body>

</html>