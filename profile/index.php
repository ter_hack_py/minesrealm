<?php
session_start();
if (!isset($_SESSION["uid"]) || $_SESSION['loggedin'] === false) {
    header("Location: /login/");
}else {
    if (isset($_SESSION['uid'])) {
        $email = $_SESSION['uid'];
        require_once("db.php");
        $sql = "SELECT * FROM users  WHERE email = '$email'";
        $result = mysqli_query($link, $sql);
        if (mysqli_num_rows($result) > 0 )  {
            while ($row = mysqli_fetch_array($result)) {
                $deposit = $row['deposit'];
                $username = $row['username'];
                $full_name = $row['full_name'];
                $email = $row["email"];
                $phone = $row['phone'];
                $bitcoin = $row['payment'];
                $id = rand("12344", "23393");
                $reg_at =  $row["reg_at"];
            }
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="We remain true to our word. By providing the best crypto currency services possible. Putting customers,partners and stake holders at the heart of our business.">
    <meta name="keywords" content="Investment, Cryptocurrency, Payment, Cashout">
    <meta name="author" content="minesrealm.com">
        
        
    <link rel="shortcut icon" href="/assets/images/2.jpeg" type="image/x-icon">
    <link rel="icon" href="/assets/images/2.jpeg" type="image/x-icon">

    <title>Mines Realm | <?php echo $username ?></title>

    <!-- Bootstrap -->
   

    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="../assets/css/magnific-popup.css" rel="stylesheet">
    <link href="../assets/css/jquery-ui.css" rel="stylesheet">


    <link href="../assets/css/animate.html" rel="stylesheet">
    <link href="../assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="../assets/css/main.css" rel="stylesheet">

    <style>
        #overlay {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.5);
        z-index: 2;
        cursor: pointer;
        }

        #text{
        position: absolute;
        top: 50%;
        left: 50%;
        font-size: 20px;
        color: white;
        transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
        }
    </style>

</head>
<body>

    <!-- Preloader -->
    <!-- <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div> -->
    <!--/Preloader -->
    
    <!--Header Top-->
<?php require_once('../header.php') ?>
   
    <!--Dashboard area-->
    <!-- justify-content-center -->
    <section class="section-padding-2 blue-bg-2">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                    <aside class="sidebar">
                        <div class="sidebar-profile">
                            <img src="/assets/images/profile.jpg" alt="">
                            <h4>Hi, <?php echo $full_name ?></h4>
                            <p>Reg: <?php echo $reg_at = date("F j, Y, g:i a");?>
               
                            </p>

                            <H3>ID #<?php echo $id ?></H3>

                        </div>
                        <ul>
                            <li class="active"><a href=""><i class="fa fa-dashboard"></i> Dashboard</a></li>
                            <!-- <li><a href="../plans/"><i class="fa fa-bookmark-o"></i> Investment Plans</a></li> -->
                            <!-- <li><a href="return-log.html"><i class="fa fa-address-book-o"></i>Return Log</a></li> -->
                            <li><a href="deposit/"><i class="fa fa-paper-plane-o"></i>Deposit Now</a></li>
                            <?php 
                                if (isset($_SESSION['uid']) && $_SESSION['uid'] == 'admin@minesrealm.com') {

                                    ?>
                                    <li><a href="users/"><i class="fa fa-paper-plane-o"></i>View All Users</a></li>
                                    <li><a href="chat/"><i class="fa fa-paper-plane-o"></i>View Chats</a></li>
                                    <li><a href="updates/"><i class="fa fa-paper-plane-o"></i>updates</a></li>
                                    <li><a href="confirm/"><i class="fa fa-paper-plane-o"></i>Confirm payment</a></li>
                                    <li><a href="messages/"><i class="fa fa-paper-plane-o"></i>Messages</a></li>
                                    <li><a href="withdrawals/"><i class="fa fa-paper-plane-o"></i>Withdrawals</a></li>
                                    <?php
                                }
                             ?>
                            <!-- <li><a href="deposite-history.html"><i class="fa fa-hourglass-end"></i>Deposite History</a></li> -->
                            <!-- <li><a href="withdraw.html"><i class="fa fa-university"></i>Withdraw</a></li> -->
                            <!-- <li><a href="withdraw-history.html"><i class="fa fa-building-o"></i>Withdraw History</a></li> -->
                            <!-- <li><a href="transaction-history.html"><i class="fa fa-cubes"></i>Transaction History</a></li> -->
                            <!-- <li><a href="referral-statistic.html"><i class="fa fa-users"></i>Referral Statistic</a></li> -->
                            <!-- <li><a href="profile.html"><i class="fa fa-user-o"></i>Edit Profile</a></li> -->
                            <!-- <li><a href="support-ticket.html"><i class="fa fa-support"></i>Support Ticket</a></li> -->
                        </ul>
                    </aside>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                    <div class="dashboard-content">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <div class="single-currency-box">
                                    <h4>Deposit Wallet</h4>
                                    <p><?php echo $deposit; if($deposit == ''){
                                        echo '00.0';
                                    } ?> USD</p>
                                    <a href="#"><i class="flaticon-save"></i></a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <div class="single-currency-box">
                                    <h4>Interest Wallet</h4>
                                    <p>0.00 USD</p>
                                    <a href="#"><i class="flaticon-save-money"></i></a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <div class="single-currency-box">
                                    <h4>Total Investment</h4>
                                    <p><?php echo $deposit; if($deposit == ''){
                                        echo '00.0';
                                    } ?> USD</p>
                                    <a href="#"><i class="flaticon-money"></i></a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <div class="single-currency-box">
                                    <h4>Total Withdraw</h4>
                                    <p>0.00 USD</p>
                                    <a href="#"><i class="flaticon-jigsaw"></i></a>
                                    <?php
                                        // if (isset($_SESSION['uid']) && $_SESSION['uid'] !== 'admin@minesrealm.com') {
                                            if (!empty($deposit)) {
                                                ?>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#withdrawal">Withdrawal request</button>
                                        <?php
                                            }
                                        // } 
                                        ?>
                                    </div>
                            </div>
                            <!-- <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <div class="single-currency-box">
                                    <h4>Total Ticket</h4>
                                    <p>0.00 USD</p>
                                    <a href="#"><i class="flaticon-3d-design"></i></a>
                                </div>
                            </div> -->
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <div class="single-currency-box">
                                    <h4>Total Deposit</h4>
                                    <p><?php echo $deposit; if($deposit == ''){
                                        echo '00.0';
                                    } ?> USD</p>
                                    <a href="#"><i class="flaticon-order"></i></a>
                                </div>
                            </div>
                            <!-- <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <div class="single-currency-box">
                                    <h4>Total Transaction</h4>
                                    <p>0.00 USD</p>
                                    <a href="#"><i class="flaticon-guarantee"></i></a>
                                </div>
                            </div> -->
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <div class="single-currency-box">
                                    <h4>Total Referral</h4>
                                    <p>0.00 USD</p>
                                    <a href="#"><i class="flaticon-cost"></i></a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <div class="single-currency-box">
                                    <h4>Total Investment</h4>
                                    <p><?php echo $deposit; if($deposit == ''){
                                        echo '00.0';
                                    } ?> USD</p>
                                    <a href="#"><i class="flaticon-money"></i></a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <div class="single-currency-box">
                                    <h4>Bonus Interest</h4>
                                    <p>10.00 USD</p>
                                    <a href="#"><i class="flaticon-jigsaw"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/Dashboard area-->
    <!--Footer Area -->
    <?php
     require_once("../footer.php")
      ?>
   <!--/Footer Area-->
    <div class="modal fade bd-example-modal-lg" id="withdrawal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Make a Withdrawal Request</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding: 2rem;">
                    <form>
                        <div class="row">
                            <div class="col-lg-12" >
                                <p id="resp_display"></p>
                                <div id='spinner' style="display:none; text-align:center; margin-left:200px" class="spinner-border text-primary" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                        <div>
                        <div class="row">
                            <!-- <div class="col-lg-5 col-sm-5 col-5">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label"><b>Plan</b></label>
                                    <select class="form-control" id="plan">
                                        <option>--Select your package--</option>
                                        <option value="b">Beginers</option>
                                        <option value="r">Regular</option>
                                        <option value="pro">Pro</option>
                                        <option value="pre">Premium</option>
                                    </select>
                                </div>
                            </div> -->
                            <div class="col-lg-6 col-sm-6 col-4">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label"><b>Current balance</b></label>
                                    <input  type="number" id="balance" class="form-control" value="<?php echo $deposit ?>" />
                                
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-4">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label"><b>Amount</b></label>
                                    <input type="number" class="form-control" id="w_amount" placeholder="Amount">
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label"><b>Wallet address</b></label>
                                    <input type="text" class="form-control" id="bitcoin_address" placeholder="Your wallet address">
                                    <input type="hidden" id="w_email" value="<?php echo $email ?>">
                                </div>
                            </div>
                        
                            <!-- <div class="col-lg-2 col-sm-2 col-2" style="display:none">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label"><b>Percentage</b></label>
                                    <input readonly="readonly" type="text" class="form-control" id="percentage" placeholder="%">
                                </div>
                            </div> -->
                            <!-- <div class="col-lg-3 col-sm-3 col-3">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label"><b>Interest</b></label>
                                    <input  type="number" class="form-control" id="interest" placeholder="Interest">
                                    
                                </div>
                            </div> -->
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-secondary" >Close</button> -->
                    <!-- <div class="modal-dialog modal-lg">...</div> -->

                    <button type="button" id="withdraw" class="bttn-small btn-fill">Done</button>
                </div>
            </div>
        </div>
    </div>

    

    <script>
        var ajaxRequest = new XMLHttpRequest(); 
        withdraw = document.querySelector("#withdraw");
        withdraw.addEventListener('click', withdrawalRequest);
        var w_amount = document.getElementById('w_amount');
    
        // var w_balance = document.getElementById('w_balance');
        // w_amount.addEventListener('input', checkAmount)
        // function checkAmount () {
        //     var w_amount = document.getElementById('w_amount');
        //     if (w_amount.value > w_balance.value) {
        //         alert('Amount is higher than your available balance');
        //     }
        // };
        
        function withdrawalRequest(e) {
            var w_amount = document.querySelector("#w_amount").value;
            var w_balance = document.getElementById('w_balance');


            // if (w_balance.value < w_amount.value) {
            //     alert('Amount is higher than your balance');
            //     return false;
            // }else{
                e.preventDefault();
                ajaxRequest.onreadystatechange = function(){
                  
                    if (ajaxRequest.readyState < 4) {
                        
                        document.getElementById('spinner').style.display='block';

                    }else if (ajaxRequest.readyState == 4) {
                        
                        document.getElementById('spinner').style.display='none';
                        resp_display.innerHTML = ajaxRequest.responseText;
                        document.getElementById('resp_display').innerHTML = ajaxRequest.responseText;
                    }
                
                }
                var w_amount = document.getElementById('w_amount');
                var w_email = document.getElementById('w_email');

                var queryString = "?email=" + w_email.value;
                queryString += "&amount=" + w_amount.value;
                queryString += "&bitcoin_address=" + bitcoin_address.value;
            
                ajaxRequest.open("GET", "https://minesrealm.com/server/request/withdraw.php" + queryString, true);
                ajaxRequest.send(null);
              
        }
        
    </script>

    <!-- Query (necessary for Bootstrap's JavaScript plugins -->
    <script src="../assets/js/jquery-3.2.1.min.js"></script>
    <script src="../assets/js/jquery-migrate.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>

    <script src="../assets/js/popper.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/owl.carousel.min.js"></script>

    <script src="../assets/js/magnific-popup.min.js"></script>
    <script src="../assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="../assets/js/isotope.pkgd.min.js"></script>
    
    <script src="../assets/js/waypoints.min.js"></script>
    <script src="../assets/js/jquery.counterup.min.js"></script>
    <script src="../assets/js/wow.min.js"></script>
    <script src="../assets/js/scrollUp.min.js"></script>

    <script src="../assets/js/script.js"></script>
</body>
</html>