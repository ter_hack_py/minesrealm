<?php
session_start();
if (!isset($_SESSION["uid"]) || $_SESSION['loggedin'] === false) {
    header("Location: /login/");
}else {
    if (isset($_SESSION['uid'])) {
        $email = $_SESSION['uid'];
        require_once("../db.php");
        $sql = "SELECT * FROM users  WHERE email = '$email'";
        $result = mysqli_query($link, $sql);
        if (mysqli_num_rows($result) > 0 )  {
            while ($row = mysqli_fetch_array($result)) {
                $username = $row['username'];
                $full_name = $row['full_name'];
                $email = $row["email"];
                $phone = $row['phone'];
                $bitcoin = $row['payment'];
                $id = rand("12344", "23393");
                $reg_at =  $row["reg_at"];
            }
        }
    }
}
if (isset($_SESSION['uid']) && $_SESSION['uid'] !== 'admin@minesrealm.com') {

    header('Location: /login/');
 }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=1024"> -->

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        
    <link rel="shortcut icon" href="/assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">

    <title>Confirm payment</title>

    <!-- Bootstrap -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/magnific-popup.css" rel="stylesheet">
    <link href="/assets/css/jquery-ui.css" rel="stylesheet">


    <link href="/assets/css/animate.html" rel="stylesheet">
    <link href="/assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="/assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        #overlay {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.5);
        z-index: 2;
        cursor: pointer;
        }

        #text{
        position: absolute;
        top: 50%;
        left: 50%;
        font-size: 20px;
        color: white;
        transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
        }
    </style>

</head>
<body>

    <!-- Preloader -->
    <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div><!--/Preloader -->
    <div class="header-top blue-bg-2">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                    <div class="single-header-top">
                        <p>
                            <span>Welcome to MinesRealm!</span>
                            <!-- <span><i class="fa fa-phone"></i> (+44) Premium only</span> -->
                        </p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                    <div class="single-header-top last">
                        <div class="account-menu">
                            <select name="country">
                                <option value="">Eng</option>
                                <option value="">Fra</option>
                                <option value="">Spn</option>
                                <option value="">Ger</option>
                            </select>
                            <ul>
                                <!-- <li><a href="">Support</a></li> -->
                                <li><a href="../logout.php"><i class="fa fa-key"></i>Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div><!--/Header Top-->
    <header class="header-area blue-bg">
        <nav class="navbar navbar-expand-lg main-menu">
            <div class="container">
                <!-- <a class="navbar-brand" href="index-2.html"><img src="/assets/images/logo-2.png" class="d-inline-block align-top" alt=""></a> -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="menu-toggle"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <!-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="blog.html" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Home</a> -->
                            <!-- <ul class="dropdown-menu"> -->
                                <!-- <li><a class="nav-item" href="index.php">Home</a></li> -->
                                
                            <!-- </ul> -->
                        <!-- </li> -->
                        <li class="nav-item"><a class="nav-link" href="/">Home</a></li>
                        
                        <li class="nav-item"><a class="nav-link" href="/interest/">Interest</a></li>
                        <li class="nav-item"><a class="nav-link" href="/about/">About</a></li>
                        <li class="nav-item"><a class="nav-link" href="/faq/">FAQ</a></li>
                        <li class="nav-item"><a class="nav-link" href="/testimonials/">Testimonials</a></li>
          
                    </ul>
                    <div class="header-btn justify-content-end">
                        <a href="../logout.php" class="bttn-small btn-fill"><i style="font-size:20px" class="fa fa-key"></i></a>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <!-- table -->
    <section class="section-padding blue-bg shaded-bg" style="z-index: 99999999999;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                    <div class="section-title cl-white">
                        <h4>Users Payment Table</h4>
                        <h2>Credit Users</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-sm-12 mb-30">
                    <div class="winner-list table-responsive">
                        <table class="table table-dark table-hover table-striped table-borderless">
                            <thead>
                              <tr>
                                <th scope="col">S/N</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">ID Number</th>
                                <th scope="col">Earnings</th>
                                <th scope="col">Min Invest</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php 
                            // define('DB_SERVER', 'localhost');
                            // define('DB_USERNAME', 'root');
                            // define('DB_PASSWORD', '');
                            // define('DB_NAME', 'minerealm');

                            // live server
                            define('DB_SERVER', 'localhost');
                            define('DB_USERNAME', 'minesrea_admin');
                            define('DB_PASSWORD', '%G^q;~_E8E7x');
                            define('DB_NAME', 'minesrea_server');   
                            
                            /* Attempt to connect to MySQL database */
                            $link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
                            
                            // Check connection
                            if($link === false){
                                die("ERROR: Could not connect. " . mysqli_connect_error());
                            }
                            $sql = "SELECT * FROM users";
                            $result = mysqli_query($link, $sql);
                            if (mysqli_num_rows($result) > 0 )  {
                                while ($row = mysqli_fetch_array($result)) {
                                    ?>
                                    <!-- update script -->
                                    
                                    <!-- stop -->
                                  <!-- start -->
                                   <!-- modal section -->
                                    
                                    <tr>
                                        <!-- <form method="post">
                                            <input type="hidden" name="id" value="<?php echo $row['id'] ?>"/>
                                        </form> -->
                                        <th scope="row"><?php echo $row['id'] ?></th>
                                        <td class="cl-yellow"><?php echo ucfirst($row['full_name']) ?></td>
                                        <td class="cl-mint"><?php echo $row['email'] ?></td>
                                        <td><?php echo '#' . $row['uid'] ?></td>
                                        <td id="deposit" class="cl-green"><?php echo $row['deposit'] ?></td>
                                        <td class="cl-red"><?php echo $row['withdrawal']?></td>
                                 
                                        <td><a class="bttn-small btn-fill" data-toggle="modal" data-target="#<?php echo 'myid'. $row['id'] ?>" data-whatever="<?php echo ucfirst($row['full_name']) ?>" href="credit.php?sid=<?php echo $row['id'] ?>">Action</a></td>
                                    </tr>
                                    <!-- new modal -->
                                    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Open modal for @mdo</button>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">Open modal for @fat</button>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Open modal for @getbootstrap</button> -->

                                    <div class="modal fade" id="<?php echo 'myid'. $row['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 99999999999;">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Credit User: <?php echo ucfirst($row['full_name']) ?></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="credit.php" method="GET">
                                                <div class="form-group">
                                                    <label for="recipient-name" class="col-form-label">Select action</label>
                                                    <select class="form-control" name="iscredit">
                                                        <option>Credit</option>
                                                        <option>Debit</option>
                                                    <select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="recipient-name" class="col-form-label">Amount:</label>
                                                    <input type="hidden" name="sid" value="<?php echo $row['id'] ?>"/>
                                                    <input type="number" class="form-control" id="recipient-name" name="amount" placeholder="$">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- new modal end -->
                                   
                                  <!-- stop -->
                                  <?php
                                }
                            }else {
                                echo 'No record found';
                            }

                            ?>
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/Winner List-->
    <?php require_once('../../footer.php') ?>
    <script>
        function off() {
            document.getElementById("overlay").style.display = "none";
        }
        let ajaxRequest; 
        
        try {
            
            ajaxRequest = new XMLHttpRequest();
        }catch (e) {
            
            try {
                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
            }catch (e) {
                try{
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }catch (e){
                    
                    alert("Your browser broke!");
                    
                }
            }
        }
       
        // const addAmount = document.getElementById('add_amount');
        const amount = document.getElementById('amount');
        
        // addAmount.addEventListener('click', submitAmount);
        // amount.addEventListener('input', displaySubmit);
        // if (amount.value = "") {
        //     addAmount.style.display = 'none';
        // }
        // function displaySubmit() {
        //     addAmount.style.display = 'block';
        // }
        function submitAmount(e) {
            var deposit = document.getElementById('deposit').innerHTML;
            var sid = document.getElementById('sid');
            e.preventDefault();
            ajaxRequest.onreadystatechange = function(){
                var resp = document.getElementById('resp');
                if (ajaxRequest.readyState < 4) {
                  
                    document.getElementById('overlay').style.display='block';
                }else if (ajaxRequest.readyState == 4) {
                    // addUser.style.display="block";
                    document.getElementById('overlay').style.display='none';
                    // console.log(queryString);
                    // showload.style.display="none";
                    resp.innerHTML = ajaxRequest.responseText;
                }
            
            }

            // var queryString = "?amount=" + amount.value;
            // queryString +=  "&sid=" + sid.value;
            // queryString += "&sid=" + sid.value;
            console.log(sid.value + amount.value);
            // ajaxRequest.open("GET", "/server/request/index.php" + queryString, true);
            // ajaxRequest.send(null);
        }
    </script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/assets/js/jquery-3.2.1.min.js"></script>
    <script src="/assets/js/jquery-migrate.js"></script>
    <script src="/assets/js/jquery-ui.js"></script>

    <script src="/assets/js/popper.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/owl.carousel.min.js"></script>

    <script src="/assets/js/magnific-popup.min.js"></script>
    <script src="/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="/assets/js/isotope.pkgd.min.js"></script>
    
    <script src="/assets/js/waypoints.min.js"></script>
    <script src="/assets/js/jquery.counterup.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/scrollUp.min.js"></script>

    <script src="/assets/js/script.js"></script>
</body>
</html>