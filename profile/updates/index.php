<?php
session_start();
if (!isset($_SESSION["uid"]) || $_SESSION['loggedin'] === false) {
    header("Location: /login/");
}else {
    if (isset($_SESSION['uid'])) {
        $email = $_SESSION['uid'];
        require_once("../db.php");
        $sql = "SELECT * FROM users  WHERE email = '$email'";
        $result = mysqli_query($link, $sql);
        if (mysqli_num_rows($result) > 0 )  {
            while ($row = mysqli_fetch_array($result)) {
                $username = $row['username'];
                $full_name = $row['full_name'];
                $email = $row["email"];
                $phone = $row['phone'];
                $bitcoin = $row['payment'];
                $id = rand("12344", "23393");
                $reg_at =  $row["reg_at"];
            }
        }
    }
}


?>
<?php 
    if (isset($_SESSION['uid']) && $_SESSION['uid'] !== 'admin@minesrealm.com') {

       header('Location: /login/');
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        
    <link rel="shortcut icon" href="/assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">

    <title>MinesRealm | <?php 
    echo $username 
    ?></title>

    <!-- Bootstrap -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/magnific-popup.css" rel="stylesheet">
    <link href="/assets/css/jquery-ui.css" rel="stylesheet">


    <link href="/assets/css/animate.html" rel="stylesheet">
    <link href="/assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="/assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        #overlay {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.5);
        z-index: 2;
        cursor: pointer;
        }

        #text{
        position: absolute;
        top: 50%;
        left: 50%;
        font-size: 20px;
        color: white;
        transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
        }
    </style>

</head>
<body>

    <!-- Preloader -->
    <!-- <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div> -->
    <!--/Preloader -->

    <!--Header Top-->
    <?php require_once('../../header.php') ?>
    <!--/Header Area-->

    

   <!--/Dashboard area-->
   <section class="section-padding-2 blue-bg-2">
        <div class="container">
            <div class="row">
                
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="row justify-content-center">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="single-brands">
                                <a href="#">
                                    <!-- <img src="/assets/images/brands/1.png" alt=""> -->
                                    <div data-toggle="modal" data-target="#creat-event" class="bttn-small btn-fill mt-30" >Add Users <i class="fa fa-plus"></i></div>
                                    <!-- <a href="#" class="btn btn-primary create-event-btn" >Record</a> -->

                                </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- modal -->
    <div class="modal fade" id="creat-event">
        <!-- overlay -->
        <div id="overlay" onclick="off()">
            <div id="text">Please wait.....</div>
        </div>

        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 id="invest" style="font-family:louis_george_cafebold"></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12" style="text-align:center" id="resp">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="m-t-20" for="val-skill">Please kindly select your category</label>
                                        <select id="i_category" class="form-control"  name="val-skill">
                                            <option value="">------ Please select Category -----</option>
                                            <option value="1">Deposit</option>
                                            <option value="2">Withdrawal</option>
                                        </select>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <!-- for deposit -->
                                    <div class="col-xl-4 col-lg-4">
                                        <input id="i_name" type="text" placeholder="Name" class="form-control" />
                                    </div>
                                    <!-- <div class="col-xl-3 col-lg-3">
                                        <button type="button" class="btn btn-default">Generate ID<span id="i_id" class="badge badge-light">2</span></button>
                                
                                    </div> -->
                                    <div class="col-xl-4 col-lg-4">
                                        <input id="i_amount" type="text" placeholder="$" value="" class="form-control" />
                                    </div>
                                    <div class="col-xl-4 col-lg-4">
                                        <button id="_add_user" class="btn btn-outline-primary">Add Investor <i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-xl-6">
                                <br>

                                    
                    
                                </div>
                                    </form>
                                <div class="col-xl-6" id="display">
                                <!-- <button  class="btn btn-danger m-t-50">Reset</button> -->

                                    <div class="col-md-12" id="showload" style="display:none">
                                        <img width="150px"  src="../assets/images/loading.gif" title='Please wait'/>
                                            Please wait
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
        function off() {
            document.getElementById("overlay").style.display = "none";
        }
        let ajaxRequest; 
        
        try {
            
            ajaxRequest = new XMLHttpRequest();
        }catch (e) {
            
            try {
                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
            }catch (e) {
                try{
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }catch (e){
                    
                    alert("Your browser broke!");
                    
                }
            }
        }
       
        const addUser = document.getElementById('_add_user');
        addUser.addEventListener('click', submitUser);
        // var i_name = document.getElementById('_name').value;
        // var i_id = document.getElementById('_id').value;
        // var i_amount = document.getElementById('amount').value;
        // var i_category = document.getElementById('category').value;
        function submitUser(e) {
            e.preventDefault();
            ajaxRequest.onreadystatechange = function(){
                

                // var display = document.getElementById("display");
                // var showload = document.getElementById('showload');
                var resp = document.getElementById('resp');

                if (ajaxRequest.readyState < 4) {
                    // showload.style.display="block";
                    // console.log('Loading data')
                    document.getElementById('overlay').style.display='block';
                }else if (ajaxRequest.readyState == 4) {
                    // addUser.style.display="block";
                    document.getElementById('overlay').style.display='none';
                    // console.log(queryString);
                    // showload.style.display="none";
                    resp.innerHTML = ajaxRequest.responseText;
                }
            
            }
           
            // const _withdrawal = document.getElementById('withdrawal');
        
            var queryString = "?i_name=" + i_name.value;
            // queryString +=  "&i_id=" + i_id.value;
            queryString += "&amount=" + i_amount.value;
            queryString += "&category=" + i_category.value;
           
            ajaxRequest.open("GET", "../../server/request/index.php" + queryString, true);
            ajaxRequest.send(null);
        }
    </script>
    <!--Footer Area -->
    <?php require_once('../../footer.php') ?>
   

   <!--/Footer Area-->

    <!-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="../js/index.js"></script> -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/assets/js/jquery-3.2.1.min.js"></script>
    <script src="/assets/js/jquery-migrate.js"></script>
    <script src="/assets/js/jquery-ui.js"></script>

    <script src="/assets/js/popper.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/owl.carousel.min.js"></script>

    <script src="/assets/js/magnific-popup.min.js"></script>
    <script src="/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="/assets/js/isotope.pkgd.min.js"></script>
    
    <script src="/assets/js/waypoints.min.js"></script>
    <script src="/assets/js/jquery.counterup.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/scrollUp.min.js"></script>

    <script src="/assets/js/script.js"></script>
   






</body>

</html>