<?php
session_start();
if (!isset($_SESSION["uid"]) || $_SESSION['loggedin'] === false) {
    header("Location: /login/");
}else {
    if (isset($_SESSION['uid'])) {
        $email = $_SESSION['uid'];
        require_once("../db.php");
        $sql = "SELECT * FROM users  WHERE email = '$email'";
        $result = mysqli_query($link, $sql);
        if (mysqli_num_rows($result) > 0 )  {
            while ($row = mysqli_fetch_array($result)) {
                $username = $row['username'];
                $full_name = $row['full_name'];
                $email = $row["email"];
                $phone = $row['phone'];
                $bitcoin = $row['payment'];
                $id = rand("12344", "23393");
                $reg_at =  $row["reg_at"];
            }
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        
    <link rel="shortcut icon" href="/assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">

    <title>Mines Realm | <?php echo $username ?></title>

    <!-- Bootstrap -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/magnific-popup.css" rel="stylesheet">
    <link href="/assets/css/jquery-ui.css" rel="stylesheet">


    <link href="/assets/css/animate.html" rel="stylesheet">
    <link href="/assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="/assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <!-- Preloader -->
    <!-- <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div> -->
    <!--/Preloader -->

    <!--Header Top-->
    <?php require_once('../../header.php') ?>
    <!--/Header Area-->

    

   <!--/Dashboard area-->
   <section class="section-padding-2 blue-bg-2">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                    <aside class="sidebar">
                        <div class="sidebar-profile">
                            <img src="/assets/images/profile.jpg" alt="">
                            <h4>Hi, <?php echo $full_name ?></h4>
                            <p>Reg: <?php echo $reg_at = date("F j, Y, g:i a") ?></p>
                            <H3>ID #<?php echo $id ?></H3>

                        </div>
                        <ul>
                            <li class="active"><a href="../"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                            <!-- <li><a href="../plans/"><i class="fa fa-bookmark-o"></i> Investment Plans</a></li> -->
                            <!-- <li><a href="return-log.html"><i class="fa fa-address-book-o"></i>Return Log</a></li> -->
                            <?php 
                                if (isset($_SESSION['uid']) && $_SESSION['uid'] == 'admin@minesrealm.com') {
                                    ?>
                                    <li><a href="/profile/users/"><i class="fa fa-paper-plane-o"></i>View All Users</a></li>
                                    <li><a href="/profile/chat/"><i class="fa fa-paper-plane-o"></i>Chats</a></li>
                                    <li><a href="/profile/updates/"><i class="fa fa-paper-plane-o"></i>updates</a></li>
                                    <li><a href="/profile/confirm/"><i class="fa fa-paper-plane-o"></i>Confirm</a></li>

                                    <?php
                                }
                             ?>
                            <li><a href=""><i class="fa fa-paper-plane-o"></i>Deposit Now</a></li>
                            <li><a href="/interest/"><i class="fa fa-paper-plane-o"></i>Calculate Interest</a></li>

                            <!-- <li><a href="deposite-history.html"><i class="fa fa-hourglass-end"></i>Deposite History</a></li>
                            <li><a href="withdraw.html"><i class="fa fa-university"></i>Withdraw</a></li>
                            <li><a href="withdraw-history.html"><i class="fa fa-building-o"></i>Withdraw History</a></li>
                            <li><a href="transaction-history.html"><i class="fa fa-cubes"></i>Transaction History</a></li> -->
                            <!-- <li><a href="referral-statistic.html"><i class="fa fa-users"></i>Referral Statistic</a></li> -->
                            <!-- <li><a href="profile.html"><i class="fa fa-user-o"></i>Edit Profile</a></li> -->
                            <!-- <li><a href="support-ticket.html"><i class="fa fa-support"></i>Support Ticket</a></li> -->
                        </ul>
                    </aside>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                    <div class="row justify-content-center">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="single-brands">
                                <a href="#">
                                    <img src="/assets/images/brands/1.png" alt="">
                                    <div data-toggle="modal" data-target="#creat-event" class="bttn-small btn-fill mt-30" >Deposit Now</div>
                                    <!-- <a href="#" class="btn btn-primary create-event-btn" >Record</a> -->

                                </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- modal -->
    <div class="modal fade" id="creat-event">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="invest" style="font-family:louis_george_cafebold"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="collapse multi-collapse" id="b_info">
                                <div class="alert alert-success alert-dismissible fade show">
                                    <!-- <h5>Beginers</h5> -->
                                    <p><strong>Beginers</strong> 2% profit of your capital investment is made daily after deposit. Same amount can not be reinvested twice in this plan.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="collapse multi-collapse" id="r_info">
                                <div class="alert alert-success alert-dismissible fade show">
                                <!-- <h5>Pro</h5> -->
                                <p><strong>Regular</strong> 4% profit of your capital investment is made daily after deposit. Same amount can not be reinvested twice in this plan. (<i><b>This plan stands a chance of weekly promotion </b></i>).</p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="collapse multi-collapse" id="p_info">
                                <div class="alert alert-success alert-dismissible fade show">
                                <!-- <h5>Expert</h5> -->

                                    <p><strong>Pro</strong> 6% profit of your capital investment is made daily after deposit. Same amount can not be reinvested twice in this plan.(<i><b>This plan stands a chance of weekly promotion </b></i>).</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="collapse multi-collapse" id="pre_info">
                                <div class="alert alert-success alert-dismissible fade show">
                                <!-- <h5>Expert</h5> -->
                                    <p> <strong>Premium</strong> 2% profit of your capital investment is made daily after deposit. Same amount can not be reinvested twice in this plan. (<i><b>This plan stands a chance of weekly promotion </b></i>).</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <form>
                                <div class="row">
                                <div class="col-md-12">
                                    <label class="m-t-20" for="val-skill">Please kindly select your plan</label>
                                    <select id="choosePlan" class="form-control"  name="val-skill">
                                        <option value="">------ Please select Plan -----</option>
                                        <option value="1">Beginers</option>
                                        
                                        <option value="2">Regular</option>
                                        <option value="3">Pro</option>
                                        <option value="4">Premium</option>

                                        <!-- <option value="4">THE SECRET OF SUCCESFULL FOREX TRADERS</option>
                                        <option value="5">FUNDRAISING FOR PROJECTS</option>
                                        <option value="6">HSC</option> -->
                                    </select>
                                    
                                </div>
                                </div>
                            <div class="row" id="wrap">
                                <!-- <p><strong>Note:</strong><i>Instyruction goes here............<i></p> -->
                                <div class="col-md-12">
                                    <br>
                                <label class="m-t-20">Amount:</label>
                                <input  style="font-weight:bold" id="amount" type="text" class="form-control" placeholder="" value="" >
                                    <label class="m-t-20"></label>
                                    <input readonly="readonly" style="font-weight:bold" id="copyText" type="text" class="form-control" placeholder="" value="1QHG18sY4yjzP9smkXDVKa9S7F9WAFvQHn" >
                                    <!-- <span class="badge badge-danger">copy</span> -->
                                </div>
                                <div class="col-md-12">
                                    <!-- <label class="m-t-20"></label> -->
                                    <br>
                                    
                                    <button type="button" id="copyNow" class="btn btn-danger m-t-50">Copy <i class="fa fa-copy"></i></button>
                                    <br><br>
                                    <div class="alert alert-success" id="proceed" style="display:none; text-align:center">
                                    <p><strong>NB:</strong> Please kindly send your Investment to the Bitcoin address and a proof of payment to: <strong>investment@minesrealm.com</strong></p>
                                    <span><a href="/profile/" class="btn btn-success">Proceed</a><span>
                                    <span><a id="cal_interest" href="javascript:void(0)" class="btn btn-success" data-toggle="modal" data-target="#calculator">Calculate Interest</a></span>
                                    </div>
                                    
                                    <!-- <input id="participant_name" type="text" class="form-control" placeholder="" value="hjdnkzxcahsmmkdnjdjndjjnnjchdjjdudnndzxi"> -->
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-xl-6">
                                <br>

                                    
                    
                                </div>
                                    </form>
                                <div class="col-xl-6" id="display">
                                <!-- <button  class="btn btn-danger m-t-50">Reset</button> -->

                                    <div class="col-md-12" id="showload" style="display:none">
                                        <img width="150px"  src="../assets/images/loading.gif" title='Please wait'/>
                                            Please wait
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--Footer Area -->
    <?php require_once('../../footer.php') ?>

   <!--/Footer Area-->




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/assets/js/jquery-3.2.1.min.js"></script>
    <script src="/assets/js/jquery-migrate.js"></script>
    <script src="/assets/js/jquery-ui.js"></script>

    <script src="/assets/js/popper.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/owl.carousel.min.js"></script>

    <script src="/assets/js/magnific-popup.min.js"></script>
    <script src="/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="/assets/js/isotope.pkgd.min.js"></script>
    
    <script src="/assets/js/waypoints.min.js"></script>
    <script src="/assets/js/jquery.counterup.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/scrollUp.min.js"></script>

    <script src="/assets/js/script.js"></script>
    <script>

        const wrap = document.getElementById('wrap');
        wrap.style.display = "none";
        const choosePlan = document.getElementById('choosePlan');
        choosePlan.addEventListener('change', plan);
        function plan() {
            const beginers = "2% daily ROI (Min: 5days)"
            const regular = "4% daily ROI (Min: 5days)";
            const pro = "6% daily ROI (Min: 5days)";
            const premium = "8% daily ROI (Min: 5days)";
            const amount = document.getElementById('amount');
            

            if (choosePlan.value == 1) {
                document.getElementById("invest").innerHTML = beginers + '<button class="btn btn-default" data-toggle="collapse" data-target="#b_info">Read more >></button>';
                wrap.style.display="block";
                amount.value = "USD 20 - 100";
            }
            else if(choosePlan.value == 2) {
                document.getElementById("invest").innerHTML = regular + '<button class="btn btn-default" data-toggle="collapse" data-target="#r_info">Read more >></button>';
                wrap.style.display="block";
                amount.value = "USD 100 - 2000";
            }
            else if (choosePlan.value == 3) {
                document.getElementById("invest").innerHTML = pro + '<button class="btn btn-default" data-toggle="collapse" data-target="#p_info">Read more >></button>';
                wrap.style.display="block";
                amount.value = "USD 1000 - 20000";
            }
            else if (choosePlan.value == 4) {
                document.getElementById("invest").innerHTML = premium + '<button class="btn btn-default" data-toggle="collapse" data-target="#pre_info">Read more >></button>';
                wrap.style.display="block";
                amount.value = "USD 2000 - 1000000";
            }
            else{
                wrap.style.display="none";
                
            }

        }
        const copyNow = document.getElementById("copyNow");
       
        copyNow.addEventListener("click", copy);
        function copy(e) {
            e.preventDefault();
            const copyText = document.getElementById("copyText");
            copyText.select();
            copyText.setSelectionRange(0, 99999);
            document.execCommand("copy");
           
            // var copyNowText = document.createTextNode('Proceed');
            // var copyNowAttr = document.createAttribute(href).value = "/profile/";
            // copyNow.setAttributeNode(copyNowAttr);
            // copyNow.appendChild(copyNowText);
            alert("Copied the: " + copyText.value);
            document.getElementById('proceed').style.display="block";

        }

        // const cal_interest = document.getElementById('cal_interest').addEventListener('click', calculate);
        // function calculate() {
        //     var txt;
        //     var r = confirm("Are you sure you want to leave this page?");
        //     if (r == true) {
        //         window.location.href = "/interest/";
        //     } 
            
           
        // }
        
        // copyText.appendChild('span')
    </script>






</body>

</html>