<!--Footer Area -->
<style>
        #overlay {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.5);
        z-index: 2;
        cursor: pointer;
        }

        #text{
        position: absolute;
        top: 50%;
        left: 50%;
        font-size: 20px;
        color: white;
        transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
        }
    </style>
<footer class="footer-area section-padding-2">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4">
                    <div class="footer-widget">
                        <h3>About Mines Realm</h3>
                        <p>With the advancement in technology & current world trends........</p>
                        <div class="social">
                            <a href="javascript:void(0)" class="facebook-bg"><i class="fa fa-facebook"></i></a>
                            <a href="javascript:void(0)" class="twitter-bg"><i class="fa fa-twitter"></i></a>
                            <a href="javascript:void(0)" class="youtube-bg"><i class="fa fa-youtube-play"></i></a>
                            <a href="javascript:void(0)" class="pinterest-bg"><i class="fa fa-pinterest-p"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4">
                    <div class="footer-widget footer-nav">
                        <h3>Links</h3>
                        <ul>
                            <li><a href="/">home</a></li>
                            <li><a href="/about/">about us</a></li>
                            <!-- <li><a href="#">Plans</a></li> -->
                            <!-- <li><a href="#"></a></li> -->
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4">
                    <div class="footer-widget footer-nav">
                        <h3>Essentials</h3>
                        <ul>
                            <li><a href="/login/">Profile</a></li>
                            
                            <li><a href="/login/">Investment</a></li>
                           
                            <li><a data-toggle="modal" data-target="#howitworks" href="javascript:void(0)">How it works</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-4">                    
                    <div class="footer-widget footer-nav">
                        <h3>Help & support</h3>
                        <ul>
                            <li><a data-toggle="modal" data-target="#contact" href="javascript:void(0)">Help Center</a></li>
                            
                            <li><a href="/faq/">FAQ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4">                    
                    <div class="footer-widget footer-nav">
                        <h3>Registered offices</h3>
                        <ul>
                            <li><a>Minesrealm</a></li>
                            
                            <li><a>king Charles Street GH14, <br> London<br> United Kingdom</a></li>
                            <li><a>100 East Speer blvd. Denver, <br> Colorado 80203 United States</a></li>
                           
                           
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start of ChatBot (www.chatbot.com) code -->
        <script type="text/javascript">
            // window.__be = window.__be || {};
            // window.__be.id = "5ec0954530007f0007245a8c";
            // (function() {
            //     var be = document.createElement('script'); be.type = 'text/javascript'; be.async = true;
            //     be.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.chatbot.com/widget/plugin.js';
            //     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(be, s);
            // })();
            
            
        </script>
        <!--Start of Tawk.to Script-->
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5ec823458ee2956d73a3b62b/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
<!--End of Tawk.to Script-->
<!--End of Tawk.to Script-->
<!-- End of ChatBot code -->

    </footer>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="copy-text">
                        <p>Copyright &copy; 2020. All rights reserved</p>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="copy-nav">
                        <ul>
                            <!-- <li><a href="#">Privacy</a></li> -->
                           
                            <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Terms & Condition</button></li>
                            
                            <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#calculator" data-whatever="@mdo">Calculate Interest Rate</button>
                            </li>
                        
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/Footer Area-->
    <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Terms & Conditions</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>We respect the privacy of users of our Service and visitors of our Site (“you”) and we are committed to protecting your online privacy. This Privacy Policy summarizes Minesrealm, Ltd.’s and its affiliated entities’ (“Minesrealm”, “we”, and “us”) procedures governing the collection, maintenance, and handling of your Personal Data.

          We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page. You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.

          1. Information Collection
          We use your Personal Data for providing and improving our hosted blockchain and mining services (the “Service”) and improving your experience of the Site. “Personal Data” means any information relating to an identified or identifiable person. We collect information you provide us when creating an account for the Service. We also use various technologies to collect and store information, including cookies, pixel tags, local storage, such as browser web storage or application data caches, databases, and server logs.

          By using the Service or accepting the terms of our Site, you agree to the collection and use of information in accordance with this Privacy Policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our terms and conditions

          1.1. Account Creation Information
          When you sign up for the Service or create an account on the Site, we collect and use the Personal Data you provide to us such as your name, email address, or billing information, credit card information, cryptocurrency wallet address, IP address, or other data that can be reasonably linked to such information by minesrealm, such as information we associate with your minesrealm Account and the Service provided to you. You will also be asked to choose a unique username and a password in order to sign up for the Service, which information will be used solely for the purpose of providing access to your user account. Upon signing up, you will have the option of securing your account by enabling the Google Authentication on Settings page. For account or password recoveries, you will be able to enter recovery email on the Settings page.

          If you are opening an account for your clients as a business user, you certify that where you have provided information regarding them you have received prior consent to provide their Personal Data to us and that you have informed them for the present Privacy Policy, to which they have agreed.

          1.2. Log Information
          Minesrealm collects information that your browser sends whenever you visit our Site and log into your account (“Log Data”). This Log Data may include information such as your computer’s Internet Protocol (“IP”) address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages and other statistics. In addition, we may use third-party services such as Google Analytics that collect, monitor and analyze this type of information in order to increase our Service’s functionality. These third-party service providers have their own privacy policies addressing how they use such information. These third-party service providers have limited access only to your Personal Data necessary to perform the tasks for which they were engaged for our behalf and are contractually bound to protect and use it only for the purposes for which it was disclosed.

          1.3. Information on Cookies
          We use “cookies” to allow the Site to recognize your browser and store user preferences and other information. Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site stored on your computer. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service. 

          2. Service Providers
          We may engage third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used. These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.

          3. Compliance with Laws
          We will disclose your Personal Data where required to do so by law or subpoena or if we believe that such action is necessary to comply with the law and the reasonable requests of law enforcement or to protect the as is set forth in the General Data Protection Regulation of the European Union. Residents of the European Union, the European Economic Area and Switzerland who visit the Site or use the Service have the following legal data protection rights under the relevant legal conditions: right to information, right to erasure, right to rectification, right to access by the data subject (Article 15 GDPR), right to deletion (Article 17 GDPR), right to correction (Article 16 GDPR), right to restriction of processing (Article 18 GDPR), right to data portability (Article 20 GDPR), right to lodge a complaint with a supervisory authority (Article 77 GDPR), right to withdraw consent (Article 7 (3) GDPR) as well as the right to object to particular data processing measures (Article 21 GDPR). These users have the right of access of their Personal Data and may at any time unsubscribe from our newsletter.

          4. Information Security
          Minesrealm has adopted reasonably designed industry appropriate data collection, storage, and processing practices and security measures, as well as physical security measures to protect against unauthorized access, alteration, disclosure or destruction of your Personal Data, username, password, transaction information and data stored in your user account. Our employees are restricted from accessing user’s names and email addresses on a need to know basis. The security of Personal Data is important to us, but we remind users that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use effective measures and tools for protection of your Personal Data, we cannot guarantee 100% security and protection of such information.

          5. International Data Transfers
          Your information, including Personal Data, may be transferred to, and maintained on, computers and systems located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction. Your consent to this Privacy Policy followed by your submission of such information represents your agreement to such transfers. We will protect the privacy and security of Personal Data we collect in accordance with this Privacy Policy, regardless of where it is processed or stored.

          6. Links to Other Sites
          Our Service may contain links to other sites that are not operated by us. If you click on a third-party link, you will be directed to that third party’s site. We strongly advise you to review the Privacy Policy of every site you visit. Minesrealm has no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites or services.

          7. Children’s Privacy
          The Site is not directed at person under the age of eighteen (18). In general, and with certain exception, privacy laws, including the General Data Protection Regulation, generally require that the collection of Personal Data of persons under the age of sixteen (16) (“Children”) requires parental consent. Further, if the member state of the European Union has not provided a lower age limit (provided that such lower age is not under 13 years), the General Data Protection Regulation obliges parents to give a consent. If you are a parent or guardian and you are aware that your Children has provided us with Personal Data, please contact us. If we discover that a person under the age of 16 has provided use us with Personal Data, we will delete such information from our servers immediately.

          8. Changes to your Personal Data
          In the event we decide to use your Personal Data for any purpose other than as stated in this Privacy Policy Statement, we will offer you an effective way to opt out of the usage of your Personal Data for those other purposes. From time to time, we may send emails to you regarding new services, releases and upcoming events (“Newsletters”) . You may opt out of receiving newsletters and other secondary messages from minesrealm by selecting the ‘unsubscribe’ function present in every email we send. However, users of the Service shall continue to receive essential Service related emails.

          9. Will my data be deleted?
          We will store your personal data as long as is necessary for the purposes set forth in this Privacy Policy and to satisfy our contractual and legal obligations. We may also store your personal data for other purposes if or as long as the law allows us store it for particular purposes, including for defense against legal claims.

          10. Newsletter
          Users may subscribe to our Newsletter through the Site to receive information about the Service and new service offerings. To receive our Newsletter, users must provide a valid email address and will, in turn, receive a confirmation email for the user to verify and authorize use of the email address as a recipient of the Newsletter. We will store the IP address and date and time of each Newsletter subscription to identify when an unauthorized third party’s email subscribed to our Newsletter. We will not collect any other data and will not transfer this information to third parties except when required legally obliged to for the establishment, exercise or defense of legal claims. You may cancel your subscription to the Newsletter at any time. You will find additional details in the subscription confirmation that we will send to your E-mail address as well as in each Newsletter.</p>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
        <button type="button" class="btn btn-primary" data-dismiss="modal">Accept</button>
      </div>
    </div>
  </div>
</div>
<!-- calculator -->
<!--  -->
<div class="modal fade" id="howitworks" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">How it works</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Each plan serves as our company's equity available to our customers and the public</p>
        <p>There are four(4) plans with a percentage of 2% - 8% daily for 5days.</p>
        <p>This means,investment are to last for 5days at least before withdrawal request can be made.</p>
        <p>Futhermore, on how this percentage's are being evaluated.
        2% daily for 5days means, you receive 2% of your investment 5 times. This equals 10% of total invesment for 5 days.</p>
        <p>2% X 5days = 10%</p>
        <p>This applies in all other plan</p>
        <p>If invesment is allowed tolast for more than 5days, the percentage of that chosen plan keep increasing. Example, Using the 2%. An investment allowed for 30days or above means,</p>
        <p>2% x 30 = 60% of total investment for 30days.</p>
        <p>2% x 60 = 90% of Total invesment for 60dyas</p>
        <p>It continues in this order, and applies to all plans.</p>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="calculator" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Interest Rate Calculator</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
            <div class="row">
                <div class="col-lg-5 col-sm-5 col-5">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"><b>Plan</b></label>
                        <select class="form-control" id="plan">
                            <option>--Select your package--</option>
                            <option value="b">Beginers</option>
                            <option value="r">Regular</option>
                            <option value="pro">Pro</option>
                            <option value="pre">Premium</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-2 col-2">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"><b>Period</b></label>
                        <input type="number" id="period" class="form-control" min="5" value="5"/>
                      
                    </div>
                </div>
                <div class="col-lg-2 col-sm-2 col-2">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"><b>Amount</b></label>
                        <input type="number" class="form-control" id="amount" placeholder="Amount">
                    </div>
                </div>
               
                <div class="col-lg-2 col-sm-2 col-2" style="display:none">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"><b>Percentage</b></label>
                        <input readonly="readonly" type="text" class="form-control" id="percentage" placeholder="%">
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-3">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"><b>Interest</b></label>
                        <input  type="number" class="form-control" id="interest" placeholder="Interest">
                        <!-- readonly="readonly" -->
                    </div>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" >Close</button> -->
        <div class="modal-dialog modal-lg">...</div>

        <button type="button" class="btn btn-info" data-dismiss="modal">Done</button>
      </div>
    </div>
  </div>
</div>

<!-- modal -->


<div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Help Center</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div id="overlay" onclick="off()">
            <div id="text">Please wait.....</div>
          </div>
          <div id="resp"></div>
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Recipient:</label>
            <input type="text" class="form-control" placeholder="Name" id="r_name">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Email:</label>
            <input type="text" class="form-control" placeholder="Email" id="r_email">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="msg"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <span>Or Send us a mail <span class="badge badge-primary">admin@minesrealm.com</span><p>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="sendMail" type="button" class="btn btn-primary">Send message</button>
      </div>
    </div>
  </div>
</div>
<script>
        // var sendMail, msg, r_name, r_email;
        sendMail = document.querySelector("#sendMail");
        sendMail.addEventListener('click', submitMail);

        function off() {
            document.getElementById("overlay").style.display = "none";
        }
        var ajaxRequest = new XMLHttpRequest(); 
        
        function submitMail(e) {
            e.preventDefault();
            ajaxRequest.onreadystatechange = function(){
                var resp = document.getElementById('resp');

                if (ajaxRequest.readyState < 4) {
                    
                    document.getElementById('overlay').style.display='block';
                }else if (ajaxRequest.readyState == 4) {
                    
                    document.getElementById('overlay').style.display='none';
                   
                    resp.innerHTML = ajaxRequest.responseText;
                }
            
            }
           
            // const _withdrawal = document.getElementById('withdrawal');

            var queryString = "?r_name=" + r_name.value;
            queryString += "&r_email=" + r_email.value;
            queryString += "&msg=" + msg.value;
           
            ajaxRequest.open("GET", "/server/request/mail.php" + queryString, true);
            ajaxRequest.send(null);
        }
    </script>
