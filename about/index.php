<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="We remain true to our word. By providing the best crypto currency services possible. Putting customers,partners and stake holders at the heart of our business.">
    <meta name="keywords" content="Investment, Cryptocurrency, Payment, Cashout">
    <meta name="author" content="minesrealm.com">
        
    <link rel="shortcut icon" href="../assets/images/2.jpeg" type="image/x-icon">
    <link rel="icon" href="../assets/images/2.jpeg" type="image/x-icon">

    <title>Minesrealm - About</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="../assets/css/magnific-popup.css" rel="stylesheet">
    <link href="../assets/css/jquery-ui.css" rel="stylesheet">


    <link href="../assets/css/animate.html" rel="stylesheet">
    <link href="../assets/css/owl.carousel.min.css" rel="stylesheet">


    <!-- Main css -->
    <link href="../assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <!-- Preloader -->
    <!-- <div class="preloader">
        <div class="lds-circle"><div></div></div>
    </div> -->
    <!--/Preloader -->

    <!--Header Top-->
    <?php require_once('../header.php') ?>
    <!--/Header Area-->
    

    <!--About Section-->
    <section class="section-padding-2 blue-bg shaded-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                    <div class="about-img mb-20">
                        <img src="../assets/images/1.jpeg" alt="">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                    <div class="about-content cl-white">
                        <div class="section-title">
                            <h4>About us</h4>
                            <h2>Play Perfect earn unlimited</h2>
                        </div>
                        <p>With the advancement in technology & current world trends, the conventional means of money & trading have been greatly altered. The same place has been taken by the stock market, foreign exchange & cryptocurrency market up to a great deal. However, an overwhelming majority of the people tend to get confused when they opt to go beyond mainstream mediums of trading & investing. This is where our platform (minesrelm.com) comes to help and enable you to be capable of using modern tools & techniques for enhancing your trading profits. Not only for newbies, but we also aim to assist the intermediates & even expert traders by providing them with all the latest gadgets which can flourish their trading & investing investments thoroughly.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 cl-white">
                    <p>We are not just another business launched for revenue motives, instead, we came into existence after extensive experience with the utmost devotion to inculcate the latest trading skills among all & sundry. Our team of professionals works untiringly to compile updated news about trade markets, newly formed technologies and tools to help you ease your burden ever so efficiently. Our range of expertise is not only confined to trading & investing only, rather provide our clients with deep insights, techniques, trends, programs, strategies & framework to excel in their trading endeavors. Regardless of the fact that you are a novice or even a thorough expert, we provide such fruitful stuff that is equally adequate for everyone to enhance their profits in a comfortable fashion. We do not believe in making unreal promises or fake commitments, instead, we prove what we claim in the pursuit of offering what we call "the best".  </p>
                    <p>We firmly believe that our clients & audience are an integral part of our business and in the same regard, we ensure to work in a foolproof manner to let you achieve the profits of your life with our premium techniques & tools. Besides, we make it certain that entire of our payment gateways & technical means work in a seamless design to be of sheer help for the potential traders. We are always keen to hear back from our clients as that is how we can elevate the level of our services. Moreover, if you have a question, query or feedback, please do not hesitate to reach us out and we will get back to you as soon as possible !!  </p>
                </div>
            </div>
        </div>
    </section><!--/About Section-->

    <!-- Testimonials Area -->
    <section class="section-padding-2 blue-bg-2">
        <div class="container">
            <div class="row">
                <div class="col centered">
                    <div class="section-title cl-white">
                        <h4>Testimonials</h4>
                        <h2>Our Player Reviews</h2>
                    </div>
                </div>
            </div>
            <div class="testimonials owl-carousel">
                <div class="single-testimonial">
                    <div class="testimonial-thumb">
                        <img src="../assets/images/reviewer/1.jpeg" alt="">
                    </div>
                    <div class="testimonial-content">
                        <div class="ratings">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                        <p>Wonderful service!! You can invest their plans right now! Site paying me fine. I absolutely trust with this company. Thanks for admin offer us a stable program! You are best!</p>
                        <div class="reviewer-meta">
                            <h5>IRMA BROCKINGTON MADISON</h5>
                            <h6>WISCONSIN</h6>
                        </div>
                    </div>
                </div>
                <div class="single-testimonial">
                    <div class="testimonial-thumb">
                        <img src="../assets/images/reviewer/2.jpg" alt="">
                    </div>
                    <div class="testimonial-content">
                        <div class="ratings">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                        <p>This is a serious paying investment program. and always pays instantly! Sent me payments without any problems. Many thanks to the admin. member</p>
                        <div class="reviewer-meta">
                            <h5>LUCAS FERRANTI</h5>
                            <h6>ORLANDO, FLORIDA</h6>
                        </div>
                    </div>
                </div>
                <div class="single-testimonial">
                    <div class="testimonial-thumb">
                        <img src="../assets/images/reviewer/3.jpg" alt="">
                    </div>
                    <div class="testimonial-content">
                        <div class="ratings">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                        <p>Great service! I have been worried about investing. But when I came here. I don't have to worry anymore. Excellent performance, they continue to make good commitments. Thank you!</p>
                        <div class="reviewer-meta">
                            <h5>LYNN SHEFFIELD WAVERLY</h5>
                            <h6>ILLINOIS</h6>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- /Testimonials Area --> 

    <!--Footer Area -->
    <?php require_once('../footer.php') ?>
    <!--/Footer Area-->




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../assets/js/jquery-3.2.1.min.js"></script>
    <script src="../assets/js/jquery-migrate.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>

    <script src="../assets/js/popper.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/owl.carousel.min.js"></script>

    <script src="../assets/js/magnific-popup.min.js"></script>
    <script src="../assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="../assets/js/isotope.pkgd.min.js"></script>
    
    <script src="../assets/js/waypoints.min.js"></script>
    <script src="../assets/js/jquery.counterup.min.js"></script>
    <script src="../assets/js/wow.min.js"></script>
    <script src="../assets/js/scrollUp.min.js"></script>
    <script src="../assets/js/script.js"></script>

</body>
</html>